﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using System.Security.Cryptography;

namespace Client {

public class LobbyManager : MonoBehaviour
{

	int[] ExpRate = {10, 100, 300, 600, 1000, 2000, 5000};

		bool ToMain = false;
		public GameObject Current;
		public GameObject Autho;
		public GameObject PickingPlayer;
		public GameObject MainCanvas;
		public GameObject Registration;
		
		public InputField LoginF;
		public InputField PassF;
		public InputField RegLoginF;
		public InputField RegPassF;
		public InputField RegEmailF;
		public Text ErrorMessage;
		
		public InputField ChatMsg;
		public Text Chat;
		public Text LobbyLogin;
		public Text LobbyLevel;
		public Text	LobbyExp;

		Manager Mg = new Manager();

		void Start() {
			Mg.Start();
			Current = Autho;
		}

		void Update() {
			Mg.Update ();
			Chat.text = SingleTone.st.Messages;
			if (Input.GetKeyDown (KeyCode.L)) {
				SwitchToMainLobby();
			}
			if (ToMain) {
				if (SingleTone.st.Login == null)
					return;
				else {
					ToMain = false;
					SwitchToMainLobby();
				}
			}
		}

		void SwitchToMainLobby() {
			Current.SetActive (false);
			LobbyExp.text = String.Format ("{0}/{1}", SingleTone.st.Exp, ExpRate[SingleTone.st.Level + 1]);
			LobbyLogin.text = SingleTone.st.Login;
			LobbyLevel.text = SingleTone.st.Level.ToString ();
			Chat.text = "";
			SingleTone.st.Messages = "";
			MainCanvas.SetActive(true);
			Current = MainCanvas;
		}

		public void getGuestAccount() {
			Mg.SendBlankMsg(2);
			ToMain = true;
		}

		public void Register() {
			Current.SetActive (false);
			Registration.SetActive (true);
			Current = Registration;
		}

		MD5CryptoServiceProvider Hasher = new MD5CryptoServiceProvider();

		public void SendRegisterRequest() {
			Hasher.ComputeHash (Encoding.UTF8.GetBytes (RegPassF.text));
			Mg.SendRegisterRequest (RegLoginF.text, Encoding.UTF8.GetString(Hasher.Hash), RegEmailF.text);
			ToMain = true;
		}

		public void SendAuthoRequest() {
			Hasher.ComputeHash (Encoding.UTF8.GetBytes (PassF.text));
			Mg.SendAuthorisationRequest (LoginF.text, Encoding.UTF8.GetString(Hasher.Hash));
			ToMain = true;
		}

		public void sendChatMsg() {
			String Msg = SingleTone.st.Login + ":: " + ChatMsg.text;
			SingleTone.st.Messages += SingleTone.st.Login + ":: " + ChatMsg.text + "\n";
			Mg.SendChatMsg (Msg, 0);
		}

		public void Quit() {
			Application.Quit ();
		}

}

}