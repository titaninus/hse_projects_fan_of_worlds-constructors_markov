using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime;


namespace Client {

public class Manager
{
		internal struct MIdent {
			public int Mid;
			public int SID;
		}

		internal class PlayerInfo {
			public int UserId;
			public string Login;
			public string Password;
			public int Level;
			public int Exp;
			public string Email;
		}

		internal class ChatInfo {
			public int ChanellId;
			public String Message;
		}

		internal class Datagramm {
			public MIdent MI = new MIdent();
			public PlayerInfo PI = null;
			public ChatInfo CI = null;
		}


			 Datagramm Out;
			 Datagramm Input;

			 public void SendBlankMsg(int Mid) {
				Out = new Datagramm ();
				Out.MI.Mid = Mid;
				CL.SendMessage (Serialize(Out));
			}

		public void SendChatMsg(String Msg, int Cid) {
			Out = new Datagramm ();
			Out.MI.Mid = 3;
			Out.MI.SID = SingleTone.st.SessionID;
			Out.CI = new ChatInfo ();
			Out.CI.ChanellId = Cid;
			Out.CI.Message = Msg;
			CL.SendMessage (Serialize(Out));
			
		}

		public void SendRegisterRequest(string Login, string Password, string Email) {
			Out = new Datagramm ();
			Out.MI.Mid = 5;
			Out.MI.SID = SingleTone.st.SessionID;
			Out.PI = new PlayerInfo ();
			Out.PI.Login = Login;
			Out.PI.Password = Password;
			Out.PI.Email = Email;
			CL.SendMessage (Serialize(Out));
		}

		public void SendAuthorisationRequest(string Login, string Password) {
			
			Out = new Datagramm ();
			Out.MI.Mid = 6;
			Out.MI.SID = SingleTone.st.SessionID;
			Out.PI = new PlayerInfo ();
			Out.PI.Login = Login;
			Out.PI.Password = Password;
			CL.SendMessage (Serialize(Out));
		}

			 String Serialize(System.Object obj) {
			return JsonConvert.SerializeObject (obj);
			}

			Datagramm Deserialize(String Msg) {
				return JsonConvert.DeserializeObject<Datagramm> (Msg);
			}

			Client CL = new Client();
			public void Start()
			{
			SingleTone.st.Messages = "";
				CL.StartConnection ();
				Console.WriteLine ("try to send init message");
				SendBlankMsg (1);
				Console.WriteLine ("Init message sended");
			}

		public void Update() {
			String Msg = CL.GetMessageForce ();
			if (Msg != null)
				ExecuteMessage (Msg);
		}
			
			 void ExecuteMessage(String Msg) {
				List<String> Msgs = new List<string> (Msg.Split ('\0'));
				foreach (String Message in Msgs) {
					if (Message.Length <= 0)
						continue;
					Datagramm Data = Deserialize (Message);
					Console.WriteLine ("Mid: {0}, SID: {1}", Data.MI.Mid, Data.MI.SID);
					switch (Data.MI.Mid) {
					case 0:
						{
							SendBlankMsg (0);
							break;}
					case 1:
						{
							SingleTone.st.SessionID = Data.MI.SID;
							Console.WriteLine ("New Session ID are binded: {0}", Data.MI.SID);
							break;
						}
				case 2: {
					PlayerInfo Pi = Data.PI;
					SingleTone.st.Login = Pi.Login;
					SingleTone.st.Exp = Pi.Exp;
					SingleTone.st.Level = Pi.Level;
					break;
				}
				case 4: {
					SingleTone.st.Messages += Data.CI.Message + "\n";
					break;
				}	 
				case 6: {
					PlayerInfo Pi = Data.PI;
					SingleTone.st.Login = Pi.Login;
					SingleTone.st.Exp = Pi.Exp;
					SingleTone.st.Level = Pi.Level;
					SingleTone.st.Email = Pi.Email;
					break;
				}
					}
				}
			}
}
}
