﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан инструментальным средством
//     В случае повторного создания кода изменения, внесенные в этот файл, будут потеряны.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

public class Manager
{
	

	public Server Serv;

	public struct MIdent {
		public int Mid;
		public int SID;
	}

	public class PlayerInfo {
		public int UserId;
		public string Login;
		public string Password;
		public int Level;
		public int Exp;
		public string Email;
	}

	public class ChatInfo {
		public int ChanellId;
		public String Message;
	}

	public class Datagramm {
		public MIdent MI = new MIdent();
		public PlayerInfo PI = null;
		public ChatInfo CI = null;
	}


	public void Init() {

	}

	public void UpdatePlayerInfo(Client Player) {

	}


	String Serialize(object obj) {
		return JsonConvert.SerializeObject (obj);
	}

	Datagramm Deserialize(String Msg) {
		return JsonConvert.DeserializeObject<Datagramm> (Msg);
	}

	public void StartCommunication(int SID) {
		Client Player = Serv.Clients [SID];
		Player.SessionID = SID;
		Player.StartConnection ();
//		InitialDatagramm ID = new InitialDatagramm ();
//		ID.Mid = 0;
//		ID.SID = SID;
//		String Msg = Serialize (ID);
//		Console.WriteLine ("Send message to client {0}: {1}", SID, Msg);
//		Player.SendMessage (Msg, 0);
	}

	public void AddInfoAndSend(Datagramm Data, int Mid, int Uid) {
		Data.MI.Mid = Mid;
		Data.MI.SID = Uid;
		String Msg = Serialize (Data);
		Console.WriteLine ("Send message to client {0}: {1}", Uid, Msg);
		Serv.Clients[Uid].SendMessage (Msg);
	}

	public void SendBlankMsg(int Mid, int id) {
		Datagramm Out = new Datagramm ();
		Out.MI.Mid = Mid;
		Out.MI.SID = id;
		String Msg = Serialize (Out);
		Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
	}

	public void SendInitialInfo (int id) {
		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 1;
		ID.MI.SID = id;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
	}

	public void SendEmptyGuest (int id) {
		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 2;
		ID.MI.SID = id;
		ID.PI = new PlayerInfo ();
		ID.PI.Login = System.String.Format ("Guest{0}", id);
		ID.PI.Exp = 0;
		ID.PI.Level = 0;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
		}

	public void SendChatMessageToAllBeside (Datagramm Data, int Beside) {
		foreach (Client player in Serv.Clients) {
			if (player.SessionID != Beside && player.IsConnected()) {
				AddInfoAndSend (Data, 4, player.SessionID);
			}
		}
	}

	public void SendChatMessageToGroupBeside (Datagramm Data, int Group, int Beside) {

	}

	public void RegisterPlayer(Datagramm Data, int id) {
		lock (Serv.CreatingPlayer) {
			PlayerDB pl = new PlayerDB ();
			StatisticsDB st = new StatisticsDB ();
			Serv.Clients [id].Login = Data.PI.Login;
			Serv.Clients [id].Password = Data.PI.Password;
			Serv.Clients [id].Exp = 0;
			Serv.Clients [id].Level = 0;
			pl.Email = Data.PI.Email;
			pl.Experience = 0;
			pl.Level = 0;
			pl.Login = Data.PI.Login;
			pl.PasswordHash = Data.PI.Password;
			st.AttackCons = 0;
			st.BlessCons = 0;
			st.CreatureCons = 0;
			st.CurseCons = 0;
			st.DiffrentConsUsed = 0;
			st.MatchesPlayed = 0;
			st.ShieldCons = 0;
			st.Wins = 0;
			Serv.Clients [id].Id = Serv.DataBase.AmountPlayers;
			pl.UserID = Serv.DataBase.AmountPlayers;
			st.UserID = Serv.DataBase.AmountPlayers;
			Serv.DataBase.CreatePlayer (pl, st);
			SendAuthoRequest (id);
		}
	
	}

	public void AuthorisatePlayer(Datagramm Data, int id) {
		PlayerDB pl = Serv.DataBase.GetPlayerInfo (Data.PI.Login);
		if (pl.PasswordHash.Equals(Data.PI.Password)) {
			Serv.Clients [id].Id = pl.UserID;
			Serv.Clients [id].Login = pl.Login;
			Serv.Clients [id].Password = pl.PasswordHash;
			Serv.Clients [id].Exp = pl.Experience;
			Serv.Clients [id].Level = pl.Level;
			Serv.Clients [id].Email = pl.Email;
			SendAuthoRequest (id);
		}
	}

	public void SendAuthoRequest(int id) {
		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 6;
		ID.MI.SID = id;
		ID.PI = new PlayerInfo ();
		ID.PI.Login = Serv.Clients [id].Login;
		ID.PI.Exp = Serv.Clients [id].Exp;
		ID.PI.Level = Serv.Clients [id].Level;
		ID.PI.Email = Serv.Clients [id].Email;
		ID.PI.Password = Serv.Clients [id].Password;
		ID.PI.UserId = Serv.Clients [id].Id;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
	}

	public void ExecuteMessage (String Msg, int id) {
		List<String> Msgs = new List<string> (Msg.Split ('\0'));
		foreach (String Message in Msgs) {
			if (Message.Length <= 0)
				continue;
			Datagramm Input = Deserialize (Message);
			switch (Input.MI.Mid) {
			case 1:
				{
					SendInitialInfo (id);
					break;
				}
			case 2:
				{
					SendEmptyGuest (id);
					break;
				}
			case 3:
				{
					SendChatMessageToAllBeside (Input, id);
					break;
				}
			case 5:
				{
					RegisterPlayer (Input, id);
					break;
				}
			case 6:
				{
					AuthorisatePlayer (Input, id);
					break;
				}
			default:
				{
					break;
				}
			}
		}
	}

}

