﻿
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using System.Threading;

namespace Client {

public class Client
{
	//private TcpClient SCI;
	//String ip = "127.0.0.1";
	String ip = "52.38.34.205";
		String host = "ec2-52-38-34-205.us-west-2.compute.amazonaws.com";
	int port = 5300;
		IPAddress ipa = null;
	NetworkStream stream;

	StreamReader sr;
	StreamWriter sw;

	private bool isConnected = false;


	public void Init() {
	}

		void LoadSettings() {
			TextAsset net = Resources.Load ("ns") as TextAsset;
			string[] Arr = net.text.Split ('\n');
			ipa = IPAddress.Parse (Arr [0].Trim());
			port = Convert.ToInt32 (Arr [1]);
		}

	public void StartConnection() {
		if (!isConnected) { 
			Console.WriteLine("Try to connect with server");
			//SCI = new TcpClient ("localhost", port);
				LoadSettings ();
				SingleTone.SCI.Connect (ipa, port);
				//stream = SingleTone.SCI.GetStream ();
			//sr = new StreamReader (stream);
			//sw = new StreamWriter (stream);
			Console.WriteLine ("Server sucsessfully connected");
			isConnected = true;
		}
	}

	public void SendMessage(string Msg) {

			///TcpClient SCI = SingleTone.SCI;
			stream = SingleTone.SCI.GetStream ();
		stream.Write (Encoding.UTF8.GetBytes (Msg + '\0'), 0, Encoding.UTF8.GetByteCount (Msg + '\0'));
		//sw.WriteLine (Msg);
	}


	private void GetMessageForceTh() {

			//TcpClient SCI = SingleTone.SCI;
			while (!SingleTone.SCI.GetStream().DataAvailable) {
			Thread.Sleep (200);
		}
	}

	public String GetMessageForce() {

			//TcpClient SCI = SingleTone.SCI;
			if (!SingleTone.SCI.GetStream ().DataAvailable)
				return null;
			byte[] Buffer = new byte[SingleTone.SCI.Available];
			SingleTone.SCI.GetStream().Read(Buffer, 0, SingleTone.SCI.Available);
		return Encoding.UTF8.GetString(Buffer);
	}

	public String TryGetMessage(bool block) {
		if (!block) {
			if (!stream.DataAvailable) {
				return null;
			}
		}
		return sr.ReadToEnd ();
	}

	private void CheckConnection() {

			TcpClient SCI = SingleTone.SCI;
		try {
			SCI.Client.Send(new byte[0]);
		}
		catch (SocketException e) {
			isConnected = false;
		}
	}

	public bool IsConnected() {
		return isConnected;
	}

	public void CloseConnection() {
			TcpClient SCI = SingleTone.SCI;

		if (isConnected) {
			SCI.Close ();
		}
		isConnected = false;
	}


}

}