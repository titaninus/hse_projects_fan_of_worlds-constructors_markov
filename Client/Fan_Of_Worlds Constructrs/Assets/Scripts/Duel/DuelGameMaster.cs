﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
namespace Client {
public class DuelGameMaster : MonoBehaviour {

	public RectTransform Content;
	public GameObject Sample;
		public Text PI;
		public Text BL;
		public Text EI;
	// Use this for initialization

	Vector3 Offset(int multiplier, Vector3 Base, Vector3 Offset) {
		return new Vector3 (Base.x + multiplier * Offset.x, Base.y + multiplier * Offset.y, Base.z + multiplier * Offset.z);
	}

	int MinOff(int objcount, int sizeofobj, int sizeofspace, int Def) {
		int Sizewithobj = (sizeofobj + sizeofspace) * objcount;
		return Mathf.Min(Sizewithobj, Def);
	}

	int ObjectsCount = 4;

	void Start () {
			SingleTone.GlobalState = SingleTone.GLState.Beginning;
			EI.text = "";
//		for (int i = 0; i < ObjectsCount; ++i) {
//
//			GameObject NewButtonG = Instantiate (Sample, Offset(-50 * i, Sample.transform.position, Sample.transform.up), Sample.transform.rotation) as GameObject;
//			Button NewButton = NewButtonG.GetComponent<Button> ();
//			NewButtonG.SetActive (true);
//			NewButton.GetComponent<RectTransform> ().SetParent (Content);
//		}
		Content.SetInsetAndSizeFromParentEdge (RectTransform.Edge.Bottom, 0,660);
	}
	
	// Update is called once per frame


	void Update () {
			//Debug.Log (SingleTone.Log);
			SingleTone.Mg.Update ();
			if (SingleTone.Q.Count >= 2) {
				Manager.BattleInfo PlBI = SingleTone.Q[0];
				Manager.BattleInfo EnBI = SingleTone.Q[1];
				SingleTone.Q.RemoveRange (0, 2);
				BL.text += PlBI.Log + '\n';
				//sSingleTone.Log = "";
				PI.text = string.Format ("{0}\n Level: {1}\n Exp:{2}\\{3}\n Health:{4}\n ActionPoints:{5}", SingleTone.Login, SingleTone.Level, SingleTone.Exp, 1000, PlBI.Health, PlBI.ActionPoints);
				EI.text = string.Format ("{0}\n Level: {1}\n Exp:{2}\\{3}\n Health:{4}\n ActionPoints:{5}", SingleTone.PartnerLogin, SingleTone.PartnerLevel, SingleTone.PartnerExp, 1000, EnBI.Health, EnBI.ActionPoints);
			}
	}

	void OnGUI() {
	}

		public void SwitchTurn() {
			if (SingleTone.NowTurnPid == SingleTone.UserId) {
				EntityMaster EM = GameObject.FindGameObjectWithTag ("EntityMaster").GetComponent<EntityMaster>();

				SingleTone.Mg.SendSwitchTurnMessage (EM.EC.Current.Scheme);
			}
		}
}
}