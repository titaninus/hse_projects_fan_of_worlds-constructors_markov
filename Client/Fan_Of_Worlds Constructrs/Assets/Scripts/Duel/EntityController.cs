
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ConsBlock{
	Start, 		// 0

	//	Basics

	Shield, 	// 1
	Creature, 	// 2
	Attack, 	// 3
	Blessing, 	// 4
	Curse, 		// 5
	Construct, 	// 6

	// Splitters

	Splitter2, 	// 7
	Splitter3,	// 8
	Splitter4,	// 9

	// Energy

	Fire, 		// 10
	Water, 		// 11
	Ground, 		// 12
	Air, 		// 13
	light,   	// 14
	Dark, 		// 15
	Magic, 		// 16

	//Effects

	Arrow, 		// 17
	Orb, 		// 18
	Wall, 		// 19
	Area, 		// 20
	Multiply,	// 21
	// blocks below can't be main effect
	PhysicalD, 	// 22
	AstralD, 	// 23
	MagicD,		// 24
	Duration, 	// 25

}


public class Stats {

	public class Resistance{
		public Resistance() {

		}

		public Resistance(float F, float W, float A, float E, float L, float D, float P, float As, float M){
			Fire = F;
			Water = W;
			Air = A;
			Earth = E;
			light = L;
			Dark = D;
			Physical = P;
			Astral = As;
			Magical = M;
		}

		public static Resistance operator + (Resistance F, Resistance Second) {
			return new Resistance (F.Fire + Second.Fire, 
				F.Water + Second.Water, 
				F.Air + Second.Air, 
				F.Earth + Second.Earth,
				F.light + Second.light,
				F.Dark + Second.Dark,
				F.Physical + Second.Physical,
				F.Astral + Second.Astral, 
				F.Magical + Second.Magical);
		}

		public static Resistance ToResist(float P) {
			return new Resistance (P, P, P, P, P, P, P, P, P);
		}
		public float Fire = 0f;
		public float Water = 0f;
		public float Air = 0f;
		public float Earth = 0f;
		public float light = 0f;
		public float Dark = 0f;
		public float Physical = 0f;
		public float Astral = 0f;
		public float Magical = 0f;
	}

	public int BaseStrength;
	public int AddStrength;
	public int Strength() {
		return BaseStrength + AddStrength;
	}

	public int BaseAgility;
	public int AddAgility;
	public int Agility() {
		return BaseAgility + AddAgility;
	}

	public int BaseIntellect;
	public int AddIntellect;
	public int Intellect() {
		return BaseIntellect + AddIntellect;
	}

	public int CurrentAddHealth;
	public int AddHealth;
	public int MaxHealth() {
		return 10 * BaseStrength + AddHealth;
	}
	public int Health() {
		return MaxHealth () - CurrentAddHealth;
	}

	public int AddDefense;
	public int Defense() {
		return BaseAgility + AddAgility;
	}

	public Resistance AddResist;
	public Resistance Resist() {
		return Resistance.ToResist(Intellect() / 10) + AddResist;
	}

	public int AddPhysDmg;
	public int PhysDmg() {
		return Strength () * 2 + AddPhysDmg;
	}

	public int AddAstDmg;
	public int AstDmg() {
		return Agility () * 2 + AddAstDmg;
	}

	public int AddMagDmg;
	public int MagDmg() {
		return Intellect () * 2 + AddMagDmg;
	}

}

public class EntityController
{

	static public int Type (ConsBlock block) {
		if (block == 0) {
			return 0;
		}
		if (block >= ConsBlock.Shield && block < ConsBlock.Splitter2)
			return 1;
		if (block >= ConsBlock.Splitter2 && block < ConsBlock.Fire)
			return 4;
		if (block >= ConsBlock.Fire && block < ConsBlock.Arrow)
			return 2;
		return 3;
	}

	static public int ConBlockAmount = 26;
		public class Entity
	{

		//Dictionary<ConsBlock, int> BlToInt = new Dictionary<ConsBlock, int>();
		static Dictionary<int, ConsBlock> IntToBl = new Dictionary<int, ConsBlock>();
		public Entity() {
			if (IntToBl.Count <= 0) {
				IntToBl.Add(0, ConsBlock.Start);
				IntToBl.Add(1, ConsBlock.Shield);
				IntToBl.Add(2, ConsBlock.Creature);
				IntToBl.Add(3, ConsBlock.Attack);
				IntToBl.Add(4, ConsBlock.Blessing);
				IntToBl.Add(5, ConsBlock.Curse);
				IntToBl.Add(6, ConsBlock.Construct);
				IntToBl.Add(7, ConsBlock.Splitter2);
				IntToBl.Add(8, ConsBlock.Splitter3);
				IntToBl.Add(9, ConsBlock.Splitter4);
				IntToBl.Add(10, ConsBlock.Fire);
				IntToBl.Add(11, ConsBlock.Water);
				IntToBl.Add(12, ConsBlock.Ground);
				IntToBl.Add(13, ConsBlock.Air);
				IntToBl.Add(14, ConsBlock.light);
				IntToBl.Add(15, ConsBlock.Dark);
				IntToBl.Add(16, ConsBlock.Magic);
				IntToBl.Add(17, ConsBlock.Arrow);
				IntToBl.Add(18, ConsBlock.Orb);
				IntToBl.Add(19, ConsBlock.Wall);
				IntToBl.Add(20, ConsBlock.Area);
				IntToBl.Add(21, ConsBlock.Multiply);
				IntToBl.Add(22, ConsBlock.PhysicalD);
				IntToBl.Add(23, ConsBlock.AstralD);
				IntToBl.Add(24, ConsBlock.MagicD);
				IntToBl.Add(25, ConsBlock.Duration);
			}
		}
		public int Strength;
		public int Intellect;
		public int Agility;

		public struct Node
		{
			public int Id;
			public ConsBlock Type;
			public int Parent;
			public int Branch;
			public List<int> Childs;
			public Node(int _id, ConsBlock _tp, int branch, int parent) {
				Id = _id;
				Type = _tp;
				Parent = parent;
				Branch = branch;
				Childs = new List<int>();
			}
		}	
		public List<Node> Nodes = new List<Node>();

		public List<int> IdToLevels = new List<int>();
		public List<List<int>> Scheme; // Level: 0 - Basic Type, 1 - Main Effect, 2 - Main Energy
		public List<int> Distances = new List<int>();

		int FindLastInBranch(int brId) {
			int id = -1;
			foreach (Node item in Nodes) {
				if (item.Branch == brId && item.Id >= id)
					id = item.Id;
			}
			return id;
		}

		public void AddBlock(int ID, int brId, int PId = -1) {
			if (PId < 0)
				PId = FindLastInBranch (brId);
			Nodes.Add (new Node (Nodes.Count, IntToBl[ID], brId, PId));
			if (PId >= 0)
				Nodes [PId].Childs.Add (Nodes.Count - 1);
			Distances.Add (0);
			Parse ();
		}

		public void DeleteBlock(int Id) {
			if (Nodes [Id].Childs.Count <= 0) {
				Nodes [Nodes [Id].Parent].Childs.Remove (Id);
				Nodes.RemoveAt (Id);
				Distances.RemoveAt (Id);
				Parse ();
			}
		}


		public bool IsSpecific = false;
		public bool HaveOwnCons = false;
		public List<Entity> OwnCons;
		public int Levels = 1;
		public int Branchs = 1;

		public void Parse() {
			Levels = 1;
			MaxDist = 0;
			Branchs = 1;
			Scheme = new List<List<int>> ();
			List<int> Tmp = new List<int> (ConBlockAmount + 3);
			for (int i = 0; i < ConBlockAmount + 3; i++) 
				Tmp.Add(0);
			Scheme.Add (Tmp);
			if(Nodes[0].Childs.Count > 0)
				Parse (0, 1, 0);
		}

		 void FindMainEnergy() {

		}

		public int MaxDist = 0;

		void Parse (int Level, int Id, int Dist) {
			ParseDfs (Level, Id, Dist);
		}

		void ParseDfs(int Level, int Id, int Dist) {
			Dist++;
			Distances [Id] = Dist;
			if (Dist > MaxDist)
				MaxDist = Dist;
			if (Id == 0) {
				Parse (Level, 1, 0);
				return;
			}
			int t = Type (Nodes [Id].Type);
			switch (t) {
			case 0:
				{
					if (Nodes[Id].Childs.Count > 0)
						ParseDfs (Level, Nodes [Id].Childs [0], Dist);
					break;
				}
			case 1:
				{
					Levels++;
					Scheme.Add (new List<int> (ConBlockAmount));
					for (int i = 0; i < ConBlockAmount + 3; i++) 
						Scheme[Levels - 1].Add(0);
					Scheme [Levels - 1][0] = (int)Nodes [Id].Type;
					Scheme [Levels - 1] [1] = -1;
					if (Nodes[Id].Childs.Count > 0)
						ParseDfs (Level, Nodes [Id].Childs [0], Dist);
					break;
				}
			case 2:
				{
					Scheme [Level] [(int)(Nodes [Id].Type)]++;
					if (Nodes[Id].Childs.Count > 0)
						ParseDfs (Level, Nodes [Id].Childs [0], Dist);
					break;
				}
			case 3:
				{
					if (Scheme [Level] [2] == -1) {
						if ((int)Nodes [Id].Type < (int)ConsBlock.PhysicalD) {
							Scheme [Level] [1] = (int)Nodes [Id].Type;
						}
					}
					Scheme [Level] [(int)Nodes [Id].Type]++;
					if (Nodes[Id].Childs.Count > 0)
						ParseDfs (Level, Nodes [Id].Childs [0], Dist);
					break;
				}
			case 4:
				{
					Branchs += (int)Nodes [Id].Type - 6;
					foreach (int n in Nodes[Id].Childs) {
						//Branchs++;
						Parse (Level, n, Dist);
					}
					break;
				}
			}


		}

	}


	public Entity Current = new Entity();
		
		public EntityController ()
		{
		}
}

public class  Interactor {
	
	public float GetDamage(EntityController.Entity Actor, Stats Target) {
		return 0f;
	}

}


