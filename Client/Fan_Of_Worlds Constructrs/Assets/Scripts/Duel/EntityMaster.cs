﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class EntityMaster : MonoBehaviour {

	public RectTransform ConBaseMenu;
	public RectTransform ConBaseBuild;

	public Texture[] Icons;

	public GameObject[] ConButtons;

	private Vector2 ConBasesLeftUpCorner;
	private float ConBasesWidth;
	private float ConBasesYOffset = 20;
	private float offsetBetwElements = 5;
	private float ConBasesXOffset;
	private float ConButtonWidth = 40;

	public EntityController EC = new EntityController();

	// Use this for initialization
	void Start () {
		ConBasesWidth = ConBaseMenu.rect.width;
		EC.Current = new EntityController.Entity ();
		EC.Current.AddBlock (0, 0);
		CurrentBlock = 0;
		CurrentBranch = 0;
		//ConBasesXOffset = -30;
		//for (int i = 0; i < ConButtons.Length; i++)
		//	PrintButtonInConBasesMenu (i);
	}
	
	// Update is called once per frame
	void Update () {

	}

	Vector2 ConBaseScroll = Vector2.zero;
	Vector2 ConBuildScroll = Vector2.zero;

	int CurrentBranch = 0;
	int CurrentBlock;

	void OnGUI() {
		ConBasesYOffset = 20;
		ConBaseScroll = GUI.BeginScrollView (new Rect(0, 400, 200, 200), ConBaseScroll, new Rect(0, 0, 200, Icons.Length * (ConButtonWidth + offsetBetwElements) + 20));
		for (int i = 0; i < Icons.Length; ++i) {
			if (GUI.Button(new Rect(10, ConBasesYOffset, ConButtonWidth, ConButtonWidth), Icons[i]))
				PrintButtonInConBuildMenu (i);
			ConBasesYOffset += offsetBetwElements + ConButtonWidth;
		}
		GUI.EndScrollView ();
		float width = Mathf.Max(EC.Current.Branchs * (ConButtonWidth + offsetBetwElements + 20), 800);
		float height = Mathf.Max(EC.Current.MaxDist * (ConButtonWidth + offsetBetwElements) + 60, 200);
		float NullBranchX = width / 2 - (ConButtonWidth + offsetBetwElements + 20) * EC.Current.Branchs / 2;
		ConBuildScroll = GUI.BeginScrollView (new Rect(220, 400, 820, 200), ConBuildScroll, new Rect(0, 0, width, height + 20));
		for (int i = 0; i < EC.Current.Nodes.Count; ++i) {
			if (GUI.Button (new Rect (NullBranchX + EC.Current.Nodes [i].Branch * (ConButtonWidth + offsetBetwElements + 20), EC.Current.Distances [i] * (ConButtonWidth + offsetBetwElements), 40, 40), Icons [(int)EC.Current.Nodes [i].Type]))
			if (i != 0) {
				EC.Current.DeleteBlock (i);
			}
			
		}
		GUI.EndScrollView ();
		GUI.Label (new Rect ((width / 2 - 60) + 220, 560, 120, 20), "current branch: " + CurrentBranch.ToString ());
		if (GUI.Button (new Rect ((width - 80) + 220, 560, 80, 20), "next branch")) {
			CurrentBranch = (CurrentBranch + 1) % EC.Current.Branchs;
		}
		if (GUI.Button (new Rect (0 + 220, 560, 80, 20), "prev branch")) {
			CurrentBranch = (CurrentBranch - 1) % EC.Current.Branchs;
		}
	}

	void PrintButtonInConBasesMenu(int ID) {
		
	}

	void PrintButtonInConBuildMenu(int ID) {
		//Debug.Log(string.Format("Print in Con Build Menu invoked with ID: {0}", ID));
		int newId = EC.Current.Nodes.Count;
		EC.Current.AddBlock(ID, CurrentBranch);
		if (ID > 6 && ID < 10) {
			for (int i = 7; i <= ID; i++) {
				EC.Current.AddBlock (0, EC.Current.Branchs - (i - 6), newId);
			}
		}
	}
}
