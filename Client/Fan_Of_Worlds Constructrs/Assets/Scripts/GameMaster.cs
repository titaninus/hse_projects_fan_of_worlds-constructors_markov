﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;

public class GameMaster : MonoBehaviour {

	//Parametrs
	public Transform recepient; 
	public Text label;

	bool isConnected = false;

	Socket s = new Socket(AddressFamily.Unspecified, SocketType.Stream, ProtocolType.Tcp); //Создаем основной сокет
	IPAddress ipAddress = null; //IP-адресс
	IPEndPoint Addr = null; //конечная точка(IP и порт)
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartConnectionHandler() {
		StartConnection ("localhost", 0, 5300);
		Console.WriteLine ("Connection sucsessful created");
		UnityEngine.Debug.Log ("Connection sucsessful created");
	}

	public void SendMessageRecHand() {
		string Msg = SendMessageRec ();
		if (Msg == null) {
			Console.WriteLine ("Message not Sended \n");
			UnityEngine.Debug.Log ("Message not Sended \n");
			return;
		}
		Console.WriteLine ("Message Sended \n" + Msg + '\n');
		UnityEngine.Debug.Log ("Message Sended \n" + Msg + '\n');
	}

	public void GetMessageRecHand() {
		string Msg = GetMessageRec ();
		if (Msg == null) {
			Console.WriteLine ("Message not Received \n");
			UnityEngine.Debug.Log ("Message not Received \n");
			return;
		}
		Console.WriteLine ("Message Received \n" + Msg + '\n');
		UnityEngine.Debug.Log ("Message Received \n" + Msg + '\n');
	}

	public void StopConnectionHandler() {
		StopConnection ();
		Console.WriteLine ("Connection sucsessful closed");
		UnityEngine.Debug.Log ("Connection sucsessful closed");
	}

	void LoadSettings(IPAddress ip, int port) {
		TextAsset net = Resources.Load ("NetSettings") as TextAsset;
		string[] Arr = net.text.Split ('\n');
		ip = IPAddress.Parse (Arr [0]);
		port = Convert.ToInt32 (Arr [1]);
	}

	void StartConnection(string Host, int IpNum, int port) {
		if (!isConnected) { 
			s = new Socket (AddressFamily.Unspecified, SocketType.Stream, ProtocolType.Tcp); //Создаем основной сокет
			//ipAddress = Dns.GetHostEntry (Host).AddressList [IpNum];
			LoadSettings(ipAddress, port);
			Addr = new IPEndPoint (ipAddress, port); 
			s.Connect (Addr); //Подключаемся к серверу
			isConnected = true;
		}
	}

	void SendMessage(string Msg) {
		s.Send (Encoding.UTF8.GetBytes (Msg));
	}

	string SendMessageRec() {
		if (!isConnected)
			return null;
		String Msg = Convert.ToString (recepient.position.x) + '$' + Convert.ToString (recepient.position.y) + '$' + Convert.ToString (recepient.position.z);
		SendMessage (Msg);
		return Msg;
	}

	string GetMessage() {
		if (!isConnected)
			return null;
		byte[] msg = new byte[s.Available];
		s.Receive(msg); //Принимаем МСГ
		String Msg = Encoding.UTF8.GetString(msg);
		return Msg;
	}

	string GetMessageRec() {
		String Msg = GetMessage ();
		label.text = Msg;
		return Msg;
	}

	void StopConnection() {
		//s.Send (Encoding.UTF8.GetBytes ("//Q//"));
		if (isConnected) s.Close ();
		isConnected = false;
	}
}
