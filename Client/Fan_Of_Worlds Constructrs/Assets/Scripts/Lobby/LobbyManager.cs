﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using System.Security.Cryptography;

namespace Client {

public class LobbyManager : MonoBehaviour
{

	int[] ExpRate = {10, 100, 300, 600, 1000, 2000, 5000};

		bool ToMain = false;
		public GameObject Current;
		public GameObject Autho;
		public GameObject MainCanvas;
		public GameObject Registration;
		public GameObject Statistics;
		
		public InputField LoginF;
		public InputField PassF;
		public InputField RegLoginF;
		public InputField RegPassF;
		public InputField RegEmailF;
		public Text ErrorMessage;
		
		public InputField ChatMsg;
		public Text Chat;
		public Text LobbyLogin;
		public Text LobbyLevel;
		public Text	LobbyExp;

		public Text FindButton;
		public GameObject InfoGO;
		public Text InfoTx;

		public Text StLogin;
		public Text StEml;
		public Text StExp;
		public Text StStr;
		public Text StInt;
		public Text StAgl;
		public Text StPld;
		public Text StWins;
		public Text StDff;
		public Text StSh;
		public Text StBles;
		public Text StCrs;
		public Text StCrt;
		public Text StAtck;




		void Awake() {

			SingleTone.Mg.Start();
			SingleTone.GlobalState = SingleTone.GLState.Win;
			Current = Autho;
		}
		void Start() {
		}

		long IntTimer = -1;

		void Update() {
			IntTimer++;
			SingleTone.Mg.Update ();
			Chat.text = SingleTone.Messages;
			if (IntTimer % 200 == 0) {
				GetOnlineStat ();
				InfoTx.text = string.Format ("Players online: {0} \nPlayers searching opponent: {1}", SingleTone.PlayersOnline, SingleTone.PlayersInQueue);
			}
			bool temp = SingleTone.B;
			if (temp) {
				SwitchToMainLobby ();
				ToMain = false;
				SingleTone.B = false;
			}
			if (SingleTone.GlobalState == SingleTone.GLState.Beginning) {
				Application.LoadLevel ("Duel");
				return;
			}
			if (ToMain) {
				if (SingleTone.Login == null)
					return;
				else {
					ToMain = false;
					SwitchToMainLobby();
				}
			}
			if (SingleTone.Flag) {
				SwtchToStat ();
				SingleTone.Flag = false;
			}
		}

		void SwitchToMainLobby() {
			Current.SetActive (false);
			int Exp = SingleTone.Exp;
			string Login = SingleTone.Login;
			int Level = SingleTone.Level;
			LobbyExp.text = String.Format ("{0}/{1}", Exp, ExpRate[SingleTone.Level + 1]);
			LobbyLogin.text = Login;
			LobbyLevel.text = Level.ToString ();
			Chat.text = "";
			SingleTone.Messages = "";
			MainCanvas.SetActive(true);
			Current = MainCanvas;
		}

		public void getGuestAccount() {
			SingleTone.Mg.SendBlankMsg(2);
			ToMain = true;
		}

		public void Register() {
			Current.SetActive (false);
			Registration.SetActive (true);
			Current = Registration;
		}

		MD5CryptoServiceProvider Hasher = new MD5CryptoServiceProvider();

		public void SendRegisterRequest() {
			Hasher.ComputeHash (Encoding.UTF8.GetBytes (RegPassF.text));
			SingleTone.Mg.SendRegisterRequest (RegLoginF.text, Encoding.UTF8.GetString(Hasher.Hash), RegEmailF.text);
			ToMain = true;
		}

		public void SendAuthoRequest() {
			Hasher.ComputeHash (Encoding.UTF8.GetBytes (PassF.text));
			SingleTone.Mg.SendAuthorisationRequest (LoginF.text, Encoding.UTF8.GetString(Hasher.Hash));
			ToMain = true;
		}

		public void sendChatMsg() {
			String Msg = SingleTone.Login + ":: " + ChatMsg.text;
			SingleTone.Messages += SingleTone.Login + ":: " + ChatMsg.text + "\n";
			SingleTone.Mg.SendChatMsg (Msg, 0);
		}

		public void GetOnlineStat() {
			SingleTone.Mg.SendOnlineRequest ();
		}

		public bool Finding = false;

		public void FindOpponentButton() {
			if (Finding) {
				Finding = false;
				FindButton.text = "Find Opponent";
				SingleTone.Mg.SendFindingPartnerRequest (Finding);
			} else {
				Finding = true;
				FindButton.text = "Cancel searching";
				SingleTone.Mg.SendFindingPartnerRequest (Finding);
			}
		}

		void SwtchToStat() {
			Current.SetActive (false);
			Manager.StatsInfo S = SingleTone.SI;
			StSh.text = S.ShieldCons.ToString ();
			StStr.text = S.Strenght.ToString ();
			StWins.text = S.Wins.ToString ();
			StAgl.text = S.Agility.ToString ();
			StPld.text = S.MatchesPlayed.ToString ();
			StLogin.text = SingleTone.Login;
			StInt.text = S.Intellect.ToString ();
			StEml.text = SingleTone.Email;
			StExp.text = SingleTone.Exp.ToString();
			StBles.text = S.BlessCons.ToString ();
			StCrs.text = S.CurseCons.ToString ();
			StCrt.text = S.CreatureCons.ToString ();
			StDff.text = S.DiffrentConsUsed.ToString ();
			StAtck.text = S.AttackCons.ToString ();

			Statistics.SetActive (true);
			Current = Statistics;
		}

		public void StatsButton() {
			SingleTone.Mg.SendStatsRequest ();
		}

		public void BackStatsButton() {

			Current.SetActive (false);
			MainCanvas.SetActive (true);
			Current = MainCanvas;
		}

		public void Mute() {
			var t = gameObject.GetComponent<MusicController> ();
			t.PlayMusic = !t.PlayMusic;
		}

		public void Quit() {
			Application.Quit ();
		}

}

}
