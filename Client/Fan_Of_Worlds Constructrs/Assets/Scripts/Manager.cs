using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime;
using UnityEngine;


namespace Client {

public class Manager
{
		public struct MIdent {
			public int Mid;
			public int SID;
		}

		public class PlayerInfo {
			public int UserId;
			public string Login;
			public string Password;
			public int Level;
			public int Exp;
			public string Email;

		}

		public class ChatInfo {
			public int ChanellId;
			public String Message;
		}

		public class OnlineInfo {
			public bool findingPartner;
			public int PlayersOnline;
			public int PlayersInQueue;
		}

		public class BattleInfo {
			public float Health;
			public int ActionPoints;
			public int NowTurnPId;
			public string Log;
		}

		public class StatsInfo {
			public int MatchesPlayed { get; set; }
			public int Wins { get; set;} 
			public int DiffrentConsUsed{get; set;}
			public int CreatureCons{get; set;}
			public int ShieldCons{get; set;}
			public int AttackCons{get; set;}
			public int CurseCons{get; set;}
			public int BlessCons{get; set;}
			public int Strenght{get; set;}
			public int Intellect{get; set;}
			public int Agility{get; set;}
		}

		public class SwitchTurnInfo {
			public List<List<int>> Scheme;
			int Target;
		}

		public class Datagramm {
			public MIdent MI = new MIdent();
			public PlayerInfo PI = null;
			public ChatInfo CI = null;
			public OnlineInfo OI = null;
			public BattleInfo PlBI = null;
			public BattleInfo EnBI = null;
			public SwitchTurnInfo STI = null;
			public StatsInfo SI = null;
		}


		public BattleInfo PlBI;
		public BattleInfo EnBI;
			 Datagramm Out;
			 Datagramm Input;

			 public void SendBlankMsg(int Mid) {
				Out = new Datagramm ();
				Out.MI.Mid = Mid;
				CL.SendMessage (Serialize(Out));
			}

		public void SendChatMsg(String Msg, int Cid) {
			Out = new Datagramm ();
			Out.MI.Mid = 3;
			Out.MI.SID = SingleTone.SessionID;
			Out.CI = new ChatInfo ();
			Out.CI.ChanellId = Cid;
			Out.CI.Message = Msg;
			CL.SendMessage (Serialize(Out));
			
		}

		public void SendFindingPartnerRequest(bool Find) {
			Out = new Datagramm ();
			Out.MI.Mid = 11;
			Out.MI.SID = SingleTone.SessionID;
			Out.OI = new OnlineInfo ();
			Out.OI.findingPartner = Find;
			CL.SendMessage (Serialize (Out));
		}

		public void SendRegisterRequest(string Login, string Password, string Email) {
			Out = new Datagramm ();
			Out.MI.Mid = 5;
			Out.MI.SID = SingleTone.SessionID;
			Out.PI = new PlayerInfo ();
			Out.PI.Login = Login;
			Out.PI.Password = Password;
			Out.PI.Email = Email;
			CL.SendMessage (Serialize(Out));
		}

		public void SendAuthorisationRequest(string Login, string Password) {
			
			Out = new Datagramm ();
			Out.MI.Mid = 6;
			Out.MI.SID = SingleTone.SessionID;
			Out.PI = new PlayerInfo ();
			Out.PI.Login = Login;
			Out.PI.Password = Password;
			CL.SendMessage (Serialize(Out));
		}

		public void SendOnlineRequest() {
			Out = new Datagramm ();
			Out.MI.Mid = 13;
			Out.MI.SID = SingleTone.SessionID;
			CL.SendMessage (Serialize (Out));
		}

		public void SendStatsRequest() {
			Out = new Datagramm ();
			Out.MI.Mid = 14;
			Out.MI.SID = SingleTone.SessionID;
			CL.SendMessage (Serialize (Out));
		}

		public void SendSwitchTurnMessage(List<List<int>> Scheme) {
			Out = new Datagramm ();
			Out.MI.Mid = 7;
			Out.MI.SID = SingleTone.SessionID;
			Out.STI = new SwitchTurnInfo ();
			Out.STI.Scheme = Scheme;
			CL.SendMessage (Serialize (Out));
		}

			 String Serialize(System.Object obj) {
			return JsonConvert.SerializeObject (obj);
			}

			Datagramm Deserialize(String Msg) {
				return JsonConvert.DeserializeObject<Datagramm> (Msg);
			}

			Client CL = new Client();
			public void Start()
			{
			SingleTone.Messages = "";
				CL.StartConnection ();
				Console.WriteLine ("try to send init message");
				SendBlankMsg (1);
				Console.WriteLine ("Init message sended");
			}

		public void Update() {
			String Msg = CL.GetMessageForce ();
			if (Msg != null)
				ExecuteMessage (Msg);
		}
			

			 void ExecuteMessage(String Msg) {
				List<String> Msgs = new List<string> (Msg.Split ('\0'));
				foreach (String Message in Msgs) {
					if (Message.Length <= 0)
						continue;
					Datagramm Data = Deserialize (Message);
					Console.WriteLine ("Mid: {0}, SID: {1}", Data.MI.Mid, Data.MI.SID);
					switch (Data.MI.Mid) {
					case 0:
						{
							SendBlankMsg (0);
							break;}
					case 1:
						{
							SingleTone.SessionID = Data.MI.SID;
							Console.WriteLine ("New Session ID are binded: {0}", Data.MI.SID);
							break;
						}
				case 2: {
					PlayerInfo Pi = Data.PI;
					SingleTone.Login = Pi.Login;
					SingleTone.Exp = Pi.Exp;
						SingleTone.Level = Pi.Level;
						SingleTone.GlobalState = SingleTone.GLState.Lobby;
						SingleTone.B = true;
					break;
				}
				case 4: {
					SingleTone.Messages += Data.CI.Message + "\n";
					break;
				}	 
				case 6: {
					PlayerInfo Pi = Data.PI;
					SingleTone.Login = Pi.Login;
					SingleTone.Exp = Pi.Exp;
					SingleTone.Level = Pi.Level;
					SingleTone.Email = Pi.Email;
						SingleTone.GlobalState = SingleTone.GLState.Lobby;
						SingleTone.B = true;
					break;
				}
				case 8:
					{
						SingleTone.Q.Add (Data.PlBI);
						SingleTone.Q.Add(Data.EnBI);
//						PlBI = Data.PlBI;
//						EnBI = Data.EnBI;
//						SingleTone.NowTurnPid = Data.PlBI.NowTurnPId;
//						SingleTone.Log = Data.PlBI.Log;
//						Debug.Log (Data.PlBI.Log);
						break;
					}
				case 12:
					{
						PlayerInfo Pi = Data.PI;
						SingleTone.PartnerLogin = Pi.Login;
						SingleTone.PartnerExp = Pi.Exp;
						SingleTone.PartnerLevel = Pi.Level;
						SingleTone.PartnerId = Pi.UserId;
						SingleTone.GlobalState = SingleTone.GLState.Beginning;
						break;
					}
				case 13:
					{
						OnlineInfo Oi = Data.OI;
						SingleTone.PlayersInQueue = Oi.PlayersInQueue;
						SingleTone.PlayersOnline = Oi.PlayersOnline;
						break;
					}
				case 14:
					{
						SingleTone.SI = Data.SI;
						SingleTone.Flag = true;
						break;
					}
					}
				}
			}
	}
}
