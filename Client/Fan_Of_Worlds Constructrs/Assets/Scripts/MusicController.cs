﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

namespace Client {

public class MusicController : MonoBehaviour {

	public bool PlayMusic;

	public AudioClip[] Musics;
	List<List<int>> Params;
	List<List<int>> Events;
	List<string> Names;
	List<string> Durations;
	List<int> Priority;

	AudioSource Src;

	enum TaskType {
		Next, 
		VolumeUp, 
		VolumeDown, 
		SwitchTo,
	}

	struct Task {
		public TaskType t;
		public float delay; // if need
		public float volume; // if need
		public int AudioID; // if need
	}

	int MusicAmount = 24;
	int currentAudioId = 5;

	// Use this for initialization
	void Start () {
		TextAsset mydata = Resources.Load("ms") as TextAsset;
		string[] Sett = mydata.text.Split ('\n');
		Params = new List<List<int>> (MusicAmount);
		for (int i = 0; i < MusicAmount; i++) {
			Params.Add (new List<int> (MusicAmount));
			for (int j = 0; j < MusicAmount; j++)
				Params [i].Add (10);
		}
		Events = new List<List<int>> (MusicAmount);
		for (int i = 0; i < MusicAmount; i++) {
			Events.Add (new List<int> (MusicAmount));
			for (int j = 0; j < MusicAmount; j++)
				Events [i].Add (0);
		}
		Names = new List<string> ();
		Durations = new List<string> ();
		for (int i = 0; i < MusicAmount; ++i) {
			string[] local = Sett [i].Split ('\t');
				List<string> Local = new  List<string> ();
				for (int j = 0; j < local.Length; j++) {
					if (local [j].Length > 0)
						Local.Add (local [j]);
				}
			Names.Add (Local [1]);
			Durations.Add (Local [2]);
			for (int j = 0; j < 6; j++) {
				Events [i] [j] = Convert.ToInt32(Local[j + 3]);
			}
			for (int j = 0; j < MusicAmount; j++) {
				Params [i] [j] = Convert.ToInt32 (Local [j + 9]);
			}
		}
		if (PlayMusic) {
			Src = gameObject.GetComponent<AudioSource> ();
			tasks = new List<Task> ();
			Priority = new List<int> (MusicAmount);
			for (int i = 0; i < MusicAmount; ++i) {
				Priority.Add (0);
			}
			Src.clip = Musics [5];
			Src.playOnAwake = true;
			Src.Play ();
			Src.loop = true;
		}
	}

	bool ForceNext;
	bool Idle = true;
	float delay;
	float curDelay;
	float VolumeStart;
	bool VolumeUp;
	float UpTo;
	bool VolumeDown;
	float DownTo;
	float curentVolume;

	List<Task> tasks;

	int EventToInt(SingleTone.GLState obj) {
		switch (obj) {
		case SingleTone.GLState.Win:
			{return 0; break;}
		case SingleTone.GLState.Lose:
			{return 1; break;}
		case SingleTone.GLState.Lobby:
			{return 2; break;}
		case SingleTone.GLState.Beginning:
			{return 3; break;}
		case SingleTone.GLState.Middle:
			{return 4; break;}
		case SingleTone.GLState.Late:
			{return 5; break;}
			default:
				{return -1; break;}
		}
	}

	Task VolumeUpTo(float volume, float delay) {
		Task tmp = new Task ();
		tmp.t = TaskType.VolumeUp;
		tmp.delay = delay;
		tmp.volume = volume;
		return tmp;
	}

	Task VolumeDownTo(float volume, float delay) {
		Task tmp = new Task ();
		tmp.t = TaskType.VolumeDown;
		tmp.delay = delay;
		tmp.volume = volume;
		return tmp;
	}
		
	Task NextTask() {
		Task tmp = new Task ();
		tmp.t = TaskType.Next;
		return tmp;
	}
		
	public void AnimateNextSong() {
		tasks.Add (VolumeDownTo (0, 1));
		tasks.Add (NextTask ());
		tasks.Add (VolumeUpTo (Src.volume, 1));
	}

	int GetMax(List<int> l) {
		int Max = int.MinValue;
		int Index = -1;
		for (int i = 0; i < l.Count; ++i) {
			if (l [i] > Max) {
				Max = l [i];
				Index = i;
			}
		}
		return Index;
	}

	void RaisePriority() {
			for (int i = 0; i < MusicAmount; ++i) {
				Priority [i] += Params [currentAudioId] [i] + Events [i] [EventToInt (SingleTone.GlobalState)];
			}
	}

		float Dur = 0f;
	
		float toSec(string Time) {
			string[] loc = Time.Split (':');
			return (float)(Convert.ToDouble(loc [0]) * 60 + Convert.ToDouble(loc [1]));
		}

	// Update is called once per frame
	void Update () {
		if (PlayMusic) {
				Dur += Time.deltaTime;
				if (Math.Abs (Dur - toSec (Durations [currentAudioId])) <= 0.5f) {
					AnimateNextSong ();
				}
			if (Idle) {
				if (tasks.Count > 0) {
					Task Current = tasks [0];
					tasks.RemoveAt (0);
					switch (Current.t) {
					case TaskType.Next:
						{
							ForceNext = true;
							Idle = false;
							break;}
					case TaskType.SwitchTo: 
						{
							Src.clip = Musics [Current.AudioID];
							break;}
					case TaskType.VolumeUp:
						{
							VolumeUp = true;
							VolumeStart = Src.volume;
							Idle = false;
							UpTo = Current.volume;
							curDelay = 0;
							delay = Current.delay;
							break;}
					default:
						{
							break;}
					}
				}
			} else {
				if (ForceNext) {
					int ind = GetMax (Priority);
					Priority [ind] = 0;
					currentAudioId = ind;
					Src.clip = Musics [ind];
					RaisePriority ();
					Src.Play ();
					ForceNext = false;
					Idle = true;
						Dur = 0f;
					return;
				}
				if (VolumeUp) {
					curDelay += Time.deltaTime;
					if (curDelay >= delay) {
						Src.volume = UpTo;
						Idle = true;
						VolumeUp = false;
						return;
					}
					Src.volume = UpTo * Mathf.FloorToInt ((curDelay / delay));
					return;
				}
				if (VolumeDown) {
					curDelay += Time.deltaTime;
					if (curDelay >= delay) {
						Src.volume = DownTo;
						Idle = true;
						VolumeDown = false;
						return;
					}
					Src.volume = VolumeStart - (VolumeStart - DownTo) * Mathf.FloorToInt ((curDelay / delay));
					return;
				}
				Idle = true;
			}
		}
	}
}
}