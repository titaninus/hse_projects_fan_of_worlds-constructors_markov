using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;

namespace Client {

	public class SingleTone : MonoBehaviour
{
		public enum GLState {
			Lobby,
			Win,
			Lose,
			Beginning,
			Middle,
			Late,
		}
	private static SingleTone ST;
	public static SingleTone st {
		get {
			if (ST == null)
				ST = new SingleTone();
			return ST;
		}
	}

		void Awake() {
			DontDestroyOnLoad (this);
		}
	protected SingleTone() {

	}
		static public int SessionID;
		static public int UserId;
		static public string Login;
		static public string Password;
		static public int Level;
		static public int Exp;
		static public string Email;
		static public string Messages;
		static public GLState GlobalState;
		static public int PartnerId;
		static public string PartnerLogin;
		static public int PartnerLevel;
		static public int PartnerExp;
		static public int PlayersOnline;
		static public int PlayersInQueue;
		static public Manager Mg = new Manager();
		static public string Log;
		static public TcpClient SCI = new TcpClient();
		static public bool B = false;
		static public int NowTurnPid;
		static public Manager.StatsInfo SI; 
		static public bool Flag = false;
		static public List<Manager.BattleInfo> Q = new  List<Manager.BattleInfo> ();

}

} 