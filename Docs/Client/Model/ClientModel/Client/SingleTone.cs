using UnityEngine;
using System.Collections;

namespace Client {

public class SingleTone
{
	private static SingleTone ST;
	public static SingleTone st {
		get {
			if (ST == null)
				ST = new SingleTone();
			return ST;
		}
	}

	protected SingleTone() {

	}
		public int SessionID;
		public int UserId;
		public string Login;
		public string Password;
		public int Level;
		public int Exp;
		public string Email;
		public string Messages;

}

} 