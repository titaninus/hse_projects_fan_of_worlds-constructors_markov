﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using System.Threading;

public class Client
{
	public TcpClient SCI;
	String ip = "127.0.0.1";
	int port = 5300;
	NetworkStream stream;

	public Server Serv;

	StreamReader sr;
	StreamWriter sw;

	private bool isConnected = false;

	void Checker(object state, System.Timers.ElapsedEventArgs e) {
		Console.WriteLine ("Client {0} timer raise", SessionID);
		try {
			Serv.ServerManager.SendBlankMsg(0, SessionID);
		}
		catch {
			isConnected = false;
			Console.WriteLine ("Client {0} - not available", SessionID);
			CloseConnection ();
		}
	}
	public System.Timers.Timer last;

	public int SessionID = -1;
	public int Id = -1;
	public string Login = null;
	public string Password = null;
	public int Level = -1;
	public int Exp = -1;
	public string Email;

	public void Init() {
		stream = SCI.GetStream ();
		sr = new StreamReader (stream);
		sw = new StreamWriter (stream);
		isConnected = true;
	}

	public void StartConnection() {
		last = new System.Timers.Timer(20000);
		last.Elapsed += Checker;
		last.AutoReset = true;
		last.Enabled = true;
	}

	public void SendMessage(string Msg) {
		stream = SCI.GetStream ();
		stream.Write (Encoding.UTF8.GetBytes (Msg + '\0'), 0, Encoding.UTF8.GetByteCount (Msg + '\0'));
		//sw.WriteLine (Convert.ToString(id) + Msg);
		//sw.WriteLine (Msg);
	}

	public void SendMessage(string Msg, byte id) {
		String ToSend = Convert.ToString(id) + Msg;
		stream = SCI.GetStream ();
		stream.Write (Encoding.UTF8.GetBytes (ToSend), 0, Encoding.UTF8.GetByteCount (ToSend));
		//sw.WriteLine (Convert.ToString(id) + Msg);
		//sw.WriteLine (Msg);
	}

	private void GetMessageForceTh() {
		while (!SCI.GetStream().DataAvailable) {
			Thread.Sleep (200);
		}
	}

	public String GetMessageForce() {
		SCI.ReceiveTimeout = 1;
		Thread waitForMessage = new Thread (GetMessageForceTh);
		waitForMessage.Start ();
		waitForMessage.Join (); 
		byte[] Buffer = new byte[SCI.Available];
		SCI.GetStream().Read(Buffer, 0, SCI.Available);
		return Encoding.UTF8.GetString(Buffer);
	}


	public String GetMessage(bool block) {
		if (!block) {
			if (!stream.DataAvailable) {
				return null;
			}
		}
		byte[] Buffer = new byte[SCI.Available];
		SCI.GetStream().Read(Buffer, 0, SCI.Available);
		return Encoding.UTF8.GetString(Buffer);
	}

//	private void CheckConnection() {
//		try {
//			SCI.Client.Send(new byte[0]);
//			isConnected = true;
//		}
//		catch (SocketException e) {
//			isConnected = false;
//			SCI.Close ();
//		}
//	}

	public bool IsConnected() {
//		if (isConnected) {
//			CheckConnection ();
//		}
		return isConnected;
	}

	public void CloseConnection() {
		if (isConnected) {
			SCI.Close ();
		}
		isConnected = false;
		last.Stop ();
		last.Dispose ();
	}

	public bool IsDataAvailable() {
		if (SCI.Connected && isConnected)
			return SCI.GetStream ().DataAvailable;
		else
			isConnected = false;
		return false;
	}

}
