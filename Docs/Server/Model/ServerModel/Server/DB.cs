﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

public class PlayerDB {
	[Key]
	public int UserID { get; set; } 
	[Key]
	public String Login { get; set; }

	public String PasswordHash { get; set; }
	public int Level { get; set; }
	public int Experience {get; set;}
	public string Email;
	public virtual StatisticsDB Stat { get; set;}

}

public class StatisticsDB {
	[Key]
	public int UserID { get; set; }

	public int MatchesPlayed { get; set; }
	public int Wins { get; set;} 

	//game-style
	public int DiffrentConsUsed;
	public int CreatureCons;
	public int ShieldCons;
	public int AttackCons;
	public int CurseCons;
	public int BlessCons;

}

public class SessionContext : DbContext {
	public DbSet<PlayerDB> Players;
	public DbSet<StatisticsDB> Stats;
}

public class DB
{
	private object DBLock = new object ();
	SessionContext db;
	public int AmountPlayers;

	public void Init() {
		db = new SessionContext ();
		db.Database.CreateIfNotExists ();
		AmountPlayers = db.Players.Count ();
	}

	public PlayerDB GetPlayerInfo(string Login) {
		lock (DBLock) {
			PlayerDB pl = (from P in db.Players
				where P.Login == Login
				select P).Single ();
			return pl;
		}
	}

	public PlayerDB GetPlayerInfo(int Uid) {
			lock (DBLock) {
				PlayerDB pl = (from P in db.Players
				                where P.UserID == Uid
				                select P).Single ();
				return pl;
			}
		}
	public void CreatePlayer(PlayerDB pl) {
		lock(DBLock) {
			db.Players.Add (pl);
			db.SaveChanges ();
		}
	}
	public void CreatePlayer(PlayerDB pl, StatisticsDB stat) {
		lock(DBLock) {
			db.Players.Add (pl);
			db.Stats.Add (stat);
			db.SaveChanges ();
		}
	}

	public void Updateinfo() {
		db.SaveChanges ();
	}

	public StatisticsDB GetPlayerStat(int Uid) {
		lock (DBLock) {
			return (from S in db.Stats
			        where S.UserID == Uid
			        select S).Single ();
		}
	}

	public  string Get(int OI, int Id)
	{
		throw new System.NotImplementedException();
	}

	public  void Set(int OI, List<String> Params)
	{
		throw new System.NotImplementedException();
	}

	public  void Create(int OI, List<String> Params)
	{
		throw new System.NotImplementedException();
	}

	public  void Update(int OI, List<String> Params)
	{
		throw new System.NotImplementedException();
	}

	public  void Delete(int OI, int Id)
	{
		throw new System.NotImplementedException();
	}

}

