﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан инструментальным средством
//     В случае повторного создания кода изменения, внесенные в этот файл, будут потеряны.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;

public class GlobalSender
{
	public virtual Server Serv
	{
		get;
		set;
	}

	public virtual List<int> MessageSended
	{
		get;
		set;
	}

	private Queue<String> QTO
	{
		get;
		set;
	}

	public Thread WorkThread;

	public void AsynchSend(string Msg, int SID) {
		Console.WriteLine ("Try to send message to player {0}, message: {1}", SID, Msg);
		Serv.Clients [SID].SCI.Send (Encoding.UTF8.GetBytes (Msg));
		Console.WriteLine ("Message to player {0} sended", SID, Msg);
	}

	public virtual void ThreadTask()
	{
		if (QTO.Count <= 0) {
			Thread.Sleep(20);
		}

		//SCI.Send (Encoding.UTF8.GetBytes ());
	}

	public  void Start()
	{
		//throw new System.NotImplementedException();
	}

}

