﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using System.Threading;

public class Client
{
	private TcpClient SCI;
	//String ip = "127.0.0.1";
	String ip = "52.58.41.124";
	int port = 5300;
	NetworkStream stream;

	String host = "ec2-52-58-41-124.eu-central-1.compute.amazonaws.com";

	StreamReader sr;
	StreamWriter sw;

	private bool isConnected = false;


	public void Init() {
	}

	public void StartConnection() {
		if (!isConnected) { 
			Console.WriteLine("Try to connect with server");
			//SCI = new TcpClient ("localhost", port);
			SCI = new TcpClient (ip, port);
			stream = SCI.GetStream ();
			sr = new StreamReader (stream);
			sw = new StreamWriter (stream);
			Console.WriteLine ("Server sucsessfully connected");
			isConnected = true;
		}
	}

	public void SendMessage(string Msg) {

		stream = SCI.GetStream ();
		stream.Write (Encoding.UTF8.GetBytes (Msg + '\0'), 0, Encoding.UTF8.GetByteCount (Msg + '\0'));
		//sw.WriteLine (Msg);
	}


	private void GetMessageForceTh() {
		while (!SCI.GetStream().DataAvailable) {
			Thread.Sleep (200);
		}
	}

	public String GetMessageForce() {
		if (!SCI.GetStream ().DataAvailable)
			return null;
		byte[] Buffer = new byte[SCI.Available];
		SCI.GetStream().Read(Buffer, 0, SCI.Available);
		return Encoding.UTF8.GetString(Buffer);
	}

	public String TryGetMessage(bool block) {
		if (!block) {
			if (!stream.DataAvailable) {
				return null;
			}
		}
		return sr.ReadToEnd ();
	}

	private void CheckConnection() {
		try {
			SCI.Client.Send(new byte[0]);
		}
		catch (SocketException e) {
			isConnected = false;
		}
	}

	public bool IsConnected() {
		return isConnected;
	}

	public void CloseConnection() {
		if (isConnected) {
			SCI.Close ();
		}
		isConnected = false;
	}

}

