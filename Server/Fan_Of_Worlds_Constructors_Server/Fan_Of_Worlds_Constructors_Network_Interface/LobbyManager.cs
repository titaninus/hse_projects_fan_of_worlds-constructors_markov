﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using System.Security.Cryptography;

public class LobbyManager : MonoBehaviour
{

	int[] ExpRate = {10, 100, 300, 600, 1000, 2000, 5000};

	public Client CSI = new Client();
	
	public List<Canvas> Interfaces;
	
	public delegate String Converter(List<String> Msg);
	public delegate void Reconverter(String Msg);
	public delegate void ReconverterListed(List<String> Arr);
	//delegate void Sender(List<String> Params);
	
	
	
	public  List<Converter> Converters;
	//public  List<Sender> Senders;
	public  List<Reconverter> Reconverters;


	
	public Canvas Autho;
	public Canvas PickingPlayer;
	public Canvas MainCanvas;
	public Canvas Registration;

	public InputField LoginF;
	public InputField PassF;
	public InputField RegLoginF;
	public InputField RegPassF;
	public InputField RegEmailF;
	public Text ErrorMessage;

	public InputField ChatMsg;
	public Text Chat;
	public Text LobbyLogin;
	public Text LobbyLevel;
	public Text	LobbyExp;

	private MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();

	String ValidConnectionConverter(String[] Params) {
		string Msg = "";
		Msg += "0\n";
		Msg += Params[0] + "\n";
		Msg += "#MSGEND#\n";
		return Msg;
	}

	void ValidConnectionSender() {
		String[] Params = {Convert.ToString(CSI.SessionID)};
		String Msg = ValidConnectionConverter (Params);
		CSI.AsynchSend (Msg);
		CSI.AsynchGet (0, ValidConnectionReconverter);
	}

	void ValidConnectionReconverter(String Msg) {
		List<String> Arr = new List<string>(Msg.Split ('\n'));
		ValidConnectionReconverter (Arr);
	}

	void ValidConnectionReconverter(List<String> Arr) {
		CSI.SessionID = int.Parse (Arr [1]);
		ValidConnectionSender ();
	}

	String AuthorizateRequestConverter( String[] Params) {
		HashProvider.Initialize ();
		HashProvider.ComputeHash (UTF8Encoding.UTF8.GetBytes (Params[0]));
		string Msg = "";
		Msg += "1\n";
		Msg += Params[1] + "\n";
		Msg += Params[2] + "\n";
		Msg += UTF8Encoding.UTF8.GetString (HashProvider.Hash) + "\n";
		Msg += "#MSGEND#\n";
		return Msg;
		SwitchToMainLobby ();
	}

	void AuthorizateRequestSender() {
		String[] Params = {PassF.text, CSI.SessionID.ToString (), LoginF.text};
		String Msg = AuthorizateRequestConverter (Params);
		CSI.AsynchSend (Msg);
		CSI.AsynchGet (1, AuthorizateRequestReconverter);
	}

	void AuthorizateRequestReconverter(String Msg) {
		List<String> Arr = new List<string>(Msg.Split ('\n'));
		AuthorizateRequestReconverter (Arr);
	}

	void AuthorizateRequestReconverter(List<String> Arr) {
		if (int.Parse (Arr [1]) == CSI.SessionID) { 
			if (Arr [3] == "0") {
				CSI.Id = int.Parse(Arr[4]);
				CSI.Level = int.Parse(Arr[5]);
				CSI.Login = LoginF.text;
				CSI.Login = LoginF.text;
			} else {
				if (Arr [3] == "1") {
					ErrorMessage.text = "Incorrect Login";
				} else {
					ErrorMessage.text = "Incorrect Password";
				}
			}
		}
	}

	String RegistrationRequestConverter( String[] Params) {
		HashProvider.Initialize ();
		HashProvider.ComputeHash (UTF8Encoding.UTF8.GetBytes (Params[0]));
		string Msg = "";
		Msg += "5\n";
		Msg += Params[1] + "\n";
		Msg += Params[2] + "\n";
		Msg += UTF8Encoding.UTF8.GetString (HashProvider.Hash) + "\n";
		Msg += Params[3] + "\n";
		Msg += "#MSGEND#\n";
		return Msg;
	}

	void RegistrationRequestSender() {
		String[] Params = {RegPassF.text, CSI.SessionID.ToString (), RegLoginF.text, RegEmailF.text};
		String Msg = RegistrationRequestConverter (Params);
		CSI.AsynchSend (Msg);
		CSI.AsynchGet (5, RegistrationRequestReconverter);
	}

	void RegistrationRequestReconverter(String Msg) {
		List<String> Arr = new List<string>(Msg.Split ('\n'));
		RegistrationRequestReconverter(Arr);
	}

	void RegistrationRequestReconverter(List<String> Arr) {
		if (int.Parse (Arr [1]) == CSI.SessionID) { 
			if (Arr [3] == "0") {
				CSI.Id = int.Parse(Arr[4]);
				CSI.Level = 0;
				CSI.Login = RegLoginF.text;
				CSI.Exp = 0;
				SwitchToMainLobby();
			} else {
				if (Arr [3] == "1") {
					ErrorMessage.text = "User with this login is already exists";
				} else {
					ErrorMessage.text = "Incorrect request";
				}
			}
		}
	}

	String GuestRequestConverter( String[] Params) {
		string Msg = "";
		Msg += "2\n";
		Msg += Params[0] + "\n";
		Msg += "#MSGEND#\n";
		return Msg;
	}

	void GuestRequestSender() {
		String[] Params = {CSI.SessionID.ToString ()};
		String Msg = GuestRequestConverter (Params);
		CSI.AsynchSend (Msg);
		CSI.AsynchGet (2, GuestRequestReconverter);
	}

	void GuestRequestReconverter( String Msg) {
		List<String> Arr = new List<string>(Msg.Split ('\n'));
		GuestRequestReconverter (Arr);
	}

	void GuestRequestReconverter( List<String> Arr) {
		CSI.Level = 0;
		CSI.Login = "Guest" + Arr [2];
		CSI.Id = int.Parse (Arr [2]);
		CSI.Exp = 0;
		SwitchToMainLobby ();
	}

	String SendMessageConverter( String[] Params) {
		string Msg = "";
		Msg += "0\n";
		Msg += Params[0] + "\n";
		Msg += "0\n";
		Msg += "1\n";
		Msg +=Params[1] + " : " + Params[2] + "\n";
		Msg += "#MSGEND#\n";
		return Msg;
	}

	void SendMessageSender() {
		String[] Params = {CSI.SessionID.ToString (), CSI.Login, ChatMsg.text};
		String Msg = RegistrationRequestConverter (Params);
		CSI.AsynchSend (Msg);
		CSI.AsynchGet (5, RegistrationRequestReconverter);
	}

	void ReceiveMessageReconverter( String Msg) {
		List<String> Arr = new List<string>(Msg.Split ('\n'));
		ReceiveMessageReconverter (Arr);
	}

	void ReceiveMessageReconverter( List<String> Arr) {
		CSI.Level = 0;
		CSI.Login = "Guest" + Arr [2];
		CSI.Id = int.Parse (Arr [2]);
		CSI.Exp = 0;
		SwitchToMainLobby ();
	}


	public virtual void Start()
	{
		CSI = new Client ();
		CSI.Init ();
		CSI.AsynchGet (4, ReceiveMessageReconverter);
		CSI.AsynchGet (0, ValidConnectionReconverter);
		CSI.StartConnection ();
//		StartCoroutine(CSI.ThreadGet());
//		StartCoroutine(CSI.ThreadSend());
	}
	
	public virtual void Update()
	{
		CSI.ThreadSend ();
		CSI.ThreadGet ();
	}
	
	public virtual void OnGUI()
	{
	}
	
	public virtual void SwitchInterface(int From, int To)
	{
	}
	
	public virtual void SendMessage(int Id, List<String> Params)
	{
	}


	void SwitchToRegistration() {
		foreach (Canvas Item in Interfaces) {
			Item.enabled = false;
		}
		Registration.enabled = true;
	}

	void SwitchToAuthorisation() {
		foreach (Canvas Item in Interfaces) {
			Item.enabled = false;
		}
		PassF.text = "";
		LoginF.text = "";
		Autho.enabled = true;

	}

	void SwitchToMainLobby() {
		foreach (Canvas Item in Interfaces) {
			Item.enabled = false;
		}
		LobbyExp.text = String.Format ("{0}/{1}", CSI.Exp, ExpRate[CSI.Level + 1]);
		LobbyLogin.text = CSI.Login;
		LobbyLevel.text = CSI.Level.ToString ();
		MainCanvas.enabled = true;
	}

	public void SingUp() {
		SwitchToRegistration ();
	}

	public void SingIn() {
		AuthorizateRequestSender ();
	}

	public void PlayAsGuest() {
		GuestRequestSender ();
	}

	public void ApplicationQuit() {
		Application.Quit ();
	}
}

