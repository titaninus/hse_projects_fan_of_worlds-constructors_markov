﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Fan_Of_Worlds_Constructors_Network_Interface
{




	public class MainClass
	{

		static String login = "Titaninus";
		static string pass = "D111111d";
		static string email = "titaninus@yandex.ru";

		public static void Main (string[] args)
		{
			CL.StartConnection ();
			Console.WriteLine ("try to send init message");
			SendBlankMsg (1);
			Console.WriteLine ("Init message sended");
			bool NeedBreak = false;
			while (!NeedBreak) {
				String Msg = CL.GetMessageForce ();
				if (Msg == null)
					continue;
				Console.WriteLine ("Msg: {0}", Msg);
				ExecuteMessage (Msg);
				Console.WriteLine ("Type Q to stop connection");
				int A = Console.Read ();
				switch (A) {
				case 'Q':
					{CL.CloseConnection (); NeedBreak = true; break;}
				case 'A':
					{SendAuthoRequest(); break;}
				case 'R':
					{SendRegisterRequest(); break;}
				case 'D':
					{

						break;
					}
				default:
					{break;}
				}
			}
			Console.WriteLine ("Press Enter to exit");
			Console.ReadLine ();
		}

		public struct MIdent {
			public int Mid;
			public int SID;
		}

		public class PlayerInfo {
			public int UserId;
			public string Login;
			public string Password;
			public int Level;
			public int Exp;
			public string Email;

		}

		public class ChatInfo {
			public int ChanellId;
			public String Message;
		}

		public class OnlineInfo {
			public bool findingPartner;
			public int PlayersOnline;
			public int PlayersInQueue;
		}

		public class BattleInfo {
			public float Health;
			public int ActionPoints;
			public int NowTurnPId;
			public string Log;
		}

		public class StatsInfo {
			public int MatchesPlayed { get; set; }
			public int Wins { get; set;} 
			public int DiffrentConsUsed{get; set;}
			public int CreatureCons{get; set;}
			public int ShieldCons{get; set;}
			public int AttackCons{get; set;}
			public int CurseCons{get; set;}
			public int BlessCons{get; set;}
			public int Strenght{get; set;}
			public int Intellect{get; set;}
			public int Agility{get; set;}
		}

		public class SwitchTurnInfo {
			public List<List<int>> Scheme;
			int Target;
		}

		public class Datagramm {
			public MIdent MI = new MIdent();
			public PlayerInfo PI = null;
			public ChatInfo CI = null;
			public OnlineInfo OI = null;
			public BattleInfo PlBI = null;
			public BattleInfo EnBI = null;
			public SwitchTurnInfo STI = null;
			public StatsInfo SI = null;
		}


		static public BattleInfo PlBI;
		static public BattleInfo EnBI;
		static Datagramm Out;
		static Datagramm Input;

		static MD5CryptoServiceProvider Hasher = new MD5CryptoServiceProvider();

		static public void SendRegisterRequest() {
			Hasher.ComputeHash (Encoding.UTF8.GetBytes (pass));
			SendRegisterRequest (login, Encoding.UTF8.GetString(Hasher.Hash), email);
			//ToMain = true;
		}

		static public void SendAuthoRequest() {
			Hasher.ComputeHash (Encoding.UTF8.GetBytes (pass));
			SendAuthorisationRequest (login, Encoding.UTF8.GetString(Hasher.Hash));
			//ToMain = true;
		}

		static public void SendBlankMsg(int Mid) {
			Out = new Datagramm ();
			Out.MI.Mid = Mid;
			CL.SendMessage (Serialize(Out));
		}

		static public void SendChatMsg(String Msg, int Cid) {
			Out = new Datagramm ();
			Out.MI.Mid = 3;
			Out.MI.SID = SingleTone.SessionID;
			Out.CI = new ChatInfo ();
			Out.CI.ChanellId = Cid;
			Out.CI.Message = Msg;
			CL.SendMessage (Serialize(Out));

		}

		static public void SendRegisterRequest(string Login, string Password, string Email) {
			Out = new Datagramm ();
			Out.MI.Mid = 5;
			Out.MI.SID = SingleTone.SessionID;
			Out.PI = new PlayerInfo ();
			Out.PI.Login = Login;
			Out.PI.Password = Password;
			Out.PI.Email = Email;
			CL.SendMessage (Serialize(Out));
		}

		static public void SendAuthorisationRequest(string Login, string Password) {

			Out = new Datagramm ();
			Out.MI.Mid = 6;
			Out.MI.SID = SingleTone.SessionID;
			Out.PI = new PlayerInfo ();
			Out.PI.Login = Login;
			Out.PI.Password = Password;
			CL.SendMessage (Serialize(Out));
		}

		bool Find = true;
		static public void SendFindingPartnerRequest(bool Find) {
			Out = new Datagramm ();
			Out.MI.Mid = 11;
			Out.MI.SID = SingleTone.SessionID;
			Out.OI = new OnlineInfo ();
			Out.OI.findingPartner = Find;
			CL.SendMessage (Serialize (Out));
		}


		static public void SendSwitchTurnMessage(List<List<int>> Scheme) {
			Out = new Datagramm ();
			Out.MI.Mid = 7;
			Out.MI.SID = SingleTone.SessionID;
			Out.STI = new SwitchTurnInfo ();
			Out.STI.Scheme = Scheme;
			CL.SendMessage (Serialize (Out));
		}

		static String Serialize(System.Object obj) {
			return JsonConvert.SerializeObject (obj);
		}

		static Datagramm Deserialize(String Msg) {
			return JsonConvert.DeserializeObject<Datagramm> (Msg);
		}

		static Client CL = new Client();
		static public void Start()
		{
			SingleTone.Messages = "";
			CL.StartConnection ();
			Console.WriteLine ("try to send init message");
			SendBlankMsg (1);
			Console.WriteLine ("Init message sended");
		}

		static public void Update() {
			String Msg = CL.GetMessageForce ();
			if (Msg != null)
				ExecuteMessage (Msg);
		}


		static void ExecuteMessage(String Msg) {
			List<String> Msgs = new List<string> (Msg.Split ('\0'));
			foreach (String Message in Msgs) {
				if (Message.Length <= 0)
					continue;
				Datagramm Data = Deserialize (Message);
				Console.WriteLine ("Mid: {0}, SID: {1}", Data.MI.Mid, Data.MI.SID);
				switch (Data.MI.Mid) {
				case 0:
					{
						SendBlankMsg (0);
						break;}
				case 1:
					{
						SingleTone.SessionID = Data.MI.SID;
						Console.WriteLine ("New Session ID are binded: {0}", Data.MI.SID);
						break;
					}
				case 2: {
						PlayerInfo Pi = Data.PI;
						SingleTone.Login = Pi.Login;
						SingleTone.Exp = Pi.Exp;
						SingleTone.Level = Pi.Level;
						SingleTone.GlobalState = SingleTone.GLState.Lobby;
						SingleTone.B = true;
						break;
					}
				case 4: {
						SingleTone.Messages += Data.CI.Message + "\n";
						break;
					}	 
				case 6: {
						PlayerInfo Pi = Data.PI;
						SingleTone.Login = Pi.Login;
						SingleTone.Exp = Pi.Exp;
						SingleTone.Level = Pi.Level;
						SingleTone.Email = Pi.Email;
						SingleTone.GlobalState = SingleTone.GLState.Lobby;
						SingleTone.B = true;
						break;
					}
				case 8:
					{
						PlBI = Data.PlBI;
						EnBI = Data.EnBI;
						SingleTone.NowTurnPid = Data.PlBI.NowTurnPId;
						SingleTone.Log = Data.PlBI.Log;
						//Debug.Log (Data.PlBI.Log);
						break;
					}
				case 12:
					{
						PlayerInfo Pi = Data.PI;
						SingleTone.PartnerLogin = Pi.Login;
						SingleTone.PartnerExp = Pi.Exp;
						SingleTone.PartnerLevel = Pi.Level;
						SingleTone.PartnerId = Pi.UserId;
						SingleTone.GlobalState = SingleTone.GLState.Beginning;
						break;
					}
				case 13:
					{
						OnlineInfo Oi = Data.OI;
						SingleTone.PlayersInQueue = Oi.PlayersInQueue;
						SingleTone.PlayersOnline = Oi.PlayersOnline;
						break;
					}
				case 14:
					{
						SingleTone.SI = Data.SI;
						SingleTone.Flag = true;
						break;
					}
				}
			}
		}
	}
}
