
using System.Collections;
namespace Fan_Of_Worlds_Constructors_Network_Interface
{
public class SingleTone
{
	private static SingleTone ST;
	public static SingleTone st {
		get {
			if (ST == null)
				ST = new SingleTone();
			return ST;
		}
	}

		public enum GLState {
			Lobby,
			Win,
			Lose,
			Beginning,
			Middle,
			Late,
		}

	protected SingleTone() {

	}
		static public int SessionID;
		static public int UserId;
		static public string Login;
		static public string Password;
		static public int Level;
		static public int Exp;
		static public string Email;
		static public string Messages;
		static public GLState GlobalState;
		static public int PartnerId;
		static public string PartnerLogin;
		static public int PartnerLevel;
		static public int PartnerExp;
		static public int PlayersOnline;
		static public int PlayersInQueue;
		static public string Log;
		static public bool B = false;
		static public int NowTurnPid;
		static public MainClass.StatsInfo SI; 
		static public bool Flag = false;


}
}
