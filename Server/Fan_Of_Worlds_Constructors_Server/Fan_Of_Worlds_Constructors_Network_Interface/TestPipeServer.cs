﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Diagnostics;
namespace Fan_Of_Worlds_Constructors_Network_Interface
{
public class PipeServer
{
	public void Start()
	{
		Process pipeClient = new Process();

		pipeClient.StartInfo.FileName = "pipeClient.exe";

		using (AnonymousPipeServerStream pipeServer =
			new AnonymousPipeServerStream(PipeDirection.InOut,
				HandleInheritability.Inheritable))
		{
			// Show that anonymous pipes do not support Message mode.
			try
			{
				Console.WriteLine("[SERVER] Setting ReadMode to \"Message\".");
				pipeServer.ReadMode = PipeTransmissionMode.Message;
			}
			catch (NotSupportedException e)
			{
				Console.WriteLine("[SERVER] Exception:\n    {0}", e.Message);
			}

			Console.WriteLine("[SERVER] Current TransmissionMode: {0}.",
				pipeServer.TransmissionMode);

			// Pass the client process a handle to the server.
			pipeClient.StartInfo.Arguments =
				pipeServer.GetClientHandleAsString();
			pipeClient.StartInfo.UseShellExecute = false;
			pipeClient.Start();

			pipeServer.DisposeLocalCopyOfClientHandle();

			try
			{
					StreamWriter sw = new StreamWriter(pipeServer);
					StreamReader sr = new StreamReader(pipeServer);
				// Read user input and send that to the client process.
				
					sw.AutoFlush = true;
					// Send a 'sync message' and wait for client to receive it.
					sw.WriteLine("SYNC");
					pipeServer.WaitForPipeDrain();
					// Send the console input to the client process.
					Console.Write("[SERVER] Enter text: ");
					sw.WriteLine(Console.ReadLine());
					Console.Write("[SERVER] Client Wrote: ");
					Console.WriteLine(sr.ReadLine());
			}
			// Catch the IOException that is raised if the pipe is broken
			// or disconnected.
			catch (IOException e)
			{
				Console.WriteLine("[SERVER] Error: {0}", e.Message);
			}
		}

		pipeClient.WaitForExit();
		pipeClient.Close();
		Console.WriteLine("[SERVER] Client quit. Server terminating.");
	}
}
}