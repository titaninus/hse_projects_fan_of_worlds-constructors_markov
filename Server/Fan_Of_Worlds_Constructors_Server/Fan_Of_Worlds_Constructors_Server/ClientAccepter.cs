﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using Fan_Of_Worlds_Constructors_Server;

public class ClientAccepter
{
	public  Client NewClient
	{
		get;
		set;
	}

	public TcpClient NewClientS;

	public  Thread WorkThread
	{
		get;
		set;
	}

	public  Server Serv
	{
		get;
		set;
	}

	public List<bool> AvailableSid = new List<bool>();

	int FirstAvailable = 0;

	public void UpdateFA() {
		FirstAvailable = AvailableSid.Count;
		for (int i = 0; i < AvailableSid.Count; ++i) {
			if (AvailableSid [i]) {
				if (i <= FirstAvailable) {
					FirstAvailable = i;
					return;
				}
			}
		}
		//FirstAvailable = AvailableSid.Count;
		AvailableSid.Add (true);
		Serv.Clients.Add (new Client());
	}

	public  void ThreadTask()
	{
		while (true) {
			if (Accepter.Pending ()) {
				lock (Serv.locker) {
					Console.WriteLine ("Try to accept new client");
					NewClientS = Accepter.AcceptTcpClient ();
					Console.WriteLine ("New client Accepted");
					Client NewClient = new Client ();
					NewClient.SCI = NewClientS;
					NewClient.Serv = Serv;
					NewClient.Init ();
					UpdateFA ();
					AvailableSid [FirstAvailable] = false;
					Serv.Clients [FirstAvailable] = NewClient;
					Serv.ServerManager.StartCommunication (FirstAvailable);
					UpdateFA ();
				}

			}
		}

	}

	TcpListener Accepter;

	public  void Start()
	{
		Accepter = Serv.SCI;
		WorkThread = new Thread (ThreadTask);
		WorkThread.Start ();
	}

}

