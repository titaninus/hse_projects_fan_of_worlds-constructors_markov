﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Npgsql;

namespace Fan_Of_Worlds_Constructors_Server
{


	[Table("Statistics", Schema = "dbo")]
	public class StatisticsDB {

		public StatisticsDB() {
		}

		public StatisticsDB(int Id, int Init) {
			StatId = Id;
			MatchesPlayed = Init;
			Wins = Init;
			DiffrentConsUsed = Init;
			CreatureCons = Init;
			ShieldCons = Init;
			AttackCons = Init;
			CurseCons = Init;
			BlessCons = Init;
		}
			

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int StatId { get; set; }

		public int MatchesPlayed { get; set; }
		public int Wins { get; set;} 

		//game-style
		public int DiffrentConsUsed{get; set;}
		public int CreatureCons{get; set;}
		public int ShieldCons{get; set;}
		public int AttackCons{get; set;}
		public int CurseCons{get; set;}
		public int BlessCons{get; set;}

	}
	[Table("SpellBook", Schema = "dbo")]
	public class SpellBookDB {
		public SpellBookDB() {
		}

		public SpellBookDB(int Id) {
			SBId = Id;
		}


		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int SBId {get; set;}

	}

	[Table("Spell", Schema = "dbo")]
	public class SpellDB {
		public SpellDB() {
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int SpellId {get; set;}
		public int[] Actions { get; set; }
		public string Name { get; set; }
		public int SpBId { get; set; }

	}


	[Table("Resist", Schema = "dbo")]
	public class ResistDB {

		public ResistDB(){

		}

		public Stats.Resistance ToResist() {
			return new Stats.Resistance ((float)Fire, (float)Water, (float)Air, (float)Earth, (float)Light, (float)Dark, (float)Physical, (float)Astral, (float)Magical);
		}

		public ResistDB(int Id, double Init) {
			ResistId = Id;
			Fire = Init;
			Water = Init;
			Air = Init;
			Earth = Init;
			Light = Init;
			Dark = Init;
			Physical = Init;
			Astral = Init;
			Magical = Init;

		}

		public ResistDB(int Id, Stats.Resistance Res) {
			ResistId = Id;
			Fire = Res.Fire;
			Water = Res.Water;
			Air = Res.Air;
			Earth = Res.Earth;
			Light = Res.light;
			Dark = Res.Dark;
			Physical = Res.Physical;
			Astral = Res.Astral;
			Magical = Res.Magical;
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ResistId{get; set;}
		public double Fire{get; set;}
		public double Water{get; set;}
		public double Air{get; set;}
		public double Earth{get; set;}
		public double Light{get; set;}
		public double Dark{get; set;}
		public double Physical{get; set;}
		public double Astral{get; set;}
		public double Magical{get; set;}
	}

	[Table("Prefs", Schema = "dbo")]
	public class PrefsDB {
		public PrefsDB() {
		}

		public Stats ToStat() {
			Stats Res = new Stats ();
			Res.AddAgility = (float)AddAgility;
			Res.AddAstDmg = (float)AddAstDmg;
			Res.AddDefense = (float)AddDefense;
			Res.AddHealth = (float)AddHealth;
			Res.AddIntellect = (float)AddIntellect;
			Res.AddMagDmg = (float)AddMagDmg;
			Res.AddPhysDmg = (float)AddPhysDmg;
			Res.AddStrength = (float)AddStrenght;
			Res.BaseAgility = (float)BaseAgility;
			Res.BaseIntellect = (float)BaseIntellect;
			Res.BaseStrength = (float)BaseStrenght;
			return Res;
		}

		public PrefsDB(int Id, float Init, int ResistId, int AddResistId) {
			PrefId = Id;
			//Resistance = ResistId;
			AddResist = AddResistId;
			BaseAgility = Init;
			BaseIntellect = Init;
			BaseStrenght = Init;
			AddAstDmg = Init;
			AddDefense = Init;
			AddHealth = Init;
			AddIntellect = Init;
			AddMagDmg = Init;
			AddPhysDmg = Init;
			AddStrenght = Init;
			AddAgility = Init;
		}

		public PrefsDB(int Id, Stats st, out ResistDB AddRes, int ResId, int AddResId) {
			PrefId = Id;
			BaseAgility = st.BaseAgility;
			BaseIntellect = st.BaseIntellect;
			BaseStrenght = st.BaseStrength;
			AddAstDmg = st.AddAstDmg;
			AddMagDmg = st.AddMagDmg;
			AddIntellect = st.AddIntellect;
			AddHealth = st.AddHealth;
			AddDefense = st.AddDefense;
			AddAgility = st.AddAgility;
			AddStrenght = st.AddStrength;
			AddRes = new ResistDB (AddResId, 0);
			//Resistance = ResId;
			AddResist = AddResId;

		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int PrefId{get; set;}
		public float BaseStrenght{get; set;}
		public float BaseAgility{get; set;}
		public float BaseIntellect{ get; set; }
		public int AddResist{get; set;}
		public float AddStrenght{get; set;}
		public float AddIntellect{get; set;}
		public float AddHealth{get; set;}
		public float AddDefense{get; set;}
		public float AddPhysDmg{get; set;}
		public float AddAstDmg{get; set;}
		public float AddMagDmg{get; set;}
		public float AddAgility { get; set; }
	}

	[Table("Media", Schema = "dbo")]
	public class MediaDB {
		public MediaDB() {
		}

		public MediaDB(int Id) {
			MediaId = Id;
			Data = "0";
			MediaType = "0";
			MediaExtention = "0";
		}
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key]
		public int MediaId{get; set;}
		public string Data{get; set;}
		public string MediaType{get; set;}
		public string MediaExtention{get; set;}
	}

[Table("Users", Schema = "dbo")]
public class PlayerDB {

		public PlayerDB() {
		}

		public PlayerDB(int Id, string L, string P, int Lv, int Ex, string Em, int SpB, int Ch, int Av, int St) {
			UserId = Id;
			Login = L;
			Password = P;
			Level = Lv;
			Exp = Ex;
			Email = Em;
			SpellBook = SpB;
			Chars = Ch;
			Avatar = Av;
			Stat = St;
		}

	[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int UserId { get; set; } 
		public String Login { get; set; }

		public String Password { get; set; }
		public int Level { get; set; }
		public int Exp {get; set;}
		public string Email { get; set; }
		public int SpellBook{get; set;}
		public int Chars{get; set;}
		public int Avatar{get; set;}
		public int Stat{get; set;}

}




public class SessionContext : DbContext {
		public DbSet<PlayerDB> Players {get; set;}
		public DbSet<StatisticsDB> Stats {get; set;}
		public DbSet<SpellBookDB> SpellBooks {get; set;}
		public DbSet<PrefsDB> Prefs {get; set;}
		public DbSet<SpellDB> Spells {get; set;}
		public DbSet<ResistDB> Resists {get; set;}
		public DbSet<MediaDB> Medias {get; set;}


		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<MediaDB> ().Property (a => a.MediaId).HasDatabaseGeneratedOption (DatabaseGeneratedOption.Identity);
		}

}




public class DB
{

		public void PrintAllPlayers() {
			Console.WriteLine ("Amount Of Players {0}", AmountPlayers);
			for (int i = 0; i < AmountPlayers; ++i) {
				var Pdb = GetPlayerInfo (i);
				Console.WriteLine ("Id: {0}, Login: {1}, Password: {2}, Email: {3}", Pdb.UserId, Pdb.Login, Pdb.Password, Pdb.Email);
			}
		}

	private object DBLock = new object ();
	SessionContext db;
	public int AmountPlayers;

	public void Init() {
		db = new SessionContext ();
			if (db.Database.Exists())
				AmountPlayers = db.Players.Count ();
			else
        		AmountPlayers = 0;
	}

	public PlayerDB GetPlayerInfo(string Login) {
		lock (DBLock) {
				var tmp = (from P in db.Players
				where P.Login == Login
				select P);
				if (tmp.Count () > 0) {
					PlayerDB pl = tmp.Single ();
					return pl;
				}
				else 
					return null;
		}
	}

	public PlayerDB GetPlayerInfo(int Uid) {
			lock (DBLock) {
				PlayerDB pl = (from P in db.Players
				                where P.UserId == Uid
				                select P).Single ();
				return pl;
			}
		}
	public void CreatePlayer(PlayerDB pl) {
		lock(DBLock) {
			db.Players.Add (pl);
			db.SaveChanges ();
		}
	}
	public void CreatePlayer(PlayerDB pl, StatisticsDB stat) {
		lock(DBLock) {
			db.Players.Add (pl);
			db.Stats.Add (stat);
			db.SaveChanges ();
		}
	}

		public void CreatePlayer(string Login, string Password, string Email, Stats st) {
			int Id = AmountPlayers;
			PlayerDB pl = new PlayerDB (Id, Login, Password, 0, 0, Email, Id, Id, Id, Id);
			StatisticsDB stdb = new StatisticsDB (Id, 0);
			SpellBookDB SbDb = new SpellBookDB (Id);
			var Mdb = db.Medias.Create ();
			Mdb = new MediaDB (Id);
			ResistDB ResDb = new ResistDB(db.Resists.Count (), 0);
			ResistDB AddResDb = new ResistDB(db.Resists.Count () + 1, 0);
			PrefsDB Pref = new PrefsDB (Id, st, out AddResDb, db.Resists.Count(), db.Resists.Count() + 1);

			db.Resists.Add (ResDb);
			db.Resists.Add (AddResDb);
			db.Prefs.Add (Pref);
			db.Medias.Add (Mdb);
			db.SpellBooks.Add (SbDb);
			db.Stats.Add (stdb);
			db.Players.Add (pl);
			db.SaveChanges ();
		}

		public void CreatePlayer(string Login, string Password, string Email) {
			int Id = AmountPlayers;
			PlayerDB pl = new PlayerDB (Id, Login, Password, 0, 0, Email, Id, Id, Id, Id);
			StatisticsDB stdb = new StatisticsDB (Id, 0);
			SpellBookDB SbDb = new SpellBookDB (Id);
			var Mdb = db.Medias.Create ();
			Mdb = new MediaDB (Id);
			ResistDB ResDb = new ResistDB(db.Resists.Count (), 0);
			ResistDB AddResDb = new ResistDB(db.Resists.Count () + 1, 0);
			PrefsDB Pref = new PrefsDB (Id, 0, db.Resists.Count(), db.Resists.Count() + 1);
			db.Resists.Add (ResDb);
			db.Resists.Add (AddResDb);
			db.Prefs.Add (Pref);
			db.Medias.Add (Mdb);
			db.SpellBooks.Add (SbDb);
			db.Stats.Add (stdb);
			db.Players.Add (pl);
			db.SaveChanges ();
		}

	public void Updateinfo() {
		db.SaveChanges ();
	}

	public StatisticsDB GetPlayerStat(int Uid) {
		lock (DBLock) {
				var Sid = (from P in db.Players
					where P.UserId == Uid
					select P).Single ().Stat;
				return (from S in db.Stats
				        where S.StatId == Sid
				        select S).Single ();
		}
	}

		public PrefsDB GetPlayerPrefs(int Uid) {
			lock (DBLock) {
				var Pid = (from P in db.Players
					where P.UserId == Uid
					select P).Single ().Stat;
				var res =  (from P in db.Prefs
				        where P.PrefId == Pid
					select P);
				return res.FirstOrDefault();
			}
		}

		public void UpdatePlayer(PlayerDB New) {
			lock (DBLock) {
				PlayerDB pl = (from P in db.Players
					where P.UserId == New.UserId
					select P).Single ();
				pl = New;
				db.SaveChanges ();
			}
		}

	public  string Get(int OI, int Id)
	{
		throw new System.NotImplementedException();
	}

	public  void Set(int OI, List<String> Params)
	{
		throw new System.NotImplementedException();
	}

	public  void Create(int OI, List<String> Params)
	{
		throw new System.NotImplementedException();
	}

	public  void Update(int OI, List<String> Params)
	{
		throw new System.NotImplementedException();
	}

	public  void Delete(int OI, int Id)
	{
		throw new System.NotImplementedException();
	}

}

}