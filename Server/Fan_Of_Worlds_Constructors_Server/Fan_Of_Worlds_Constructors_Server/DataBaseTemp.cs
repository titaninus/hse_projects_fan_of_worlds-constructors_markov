﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace Fan_Of_Worlds_Constructors_Server
{

	//[Table(name = "Players", Schema = "public")]
	public class PlayerDB {
		public int UserID { get; set; } 
		public String Login { get; set; }

		public String PasswordHash { get; set; }
		public int Level { get; set; }
		public int Experience {get; set;}
		public string Email;
		public virtual StatisticsDB Stat { get; set;}

	}

	//[Table("Statistics", Schema = "public")]
	public class StatisticsDB {
		public int UserID { get; set; }

		public int MatchesPlayed { get; set; }
		public int Wins { get; set;} 

		//game-style
		public int DiffrentConsUsed;
		public int CreatureCons;
		public int ShieldCons;
		public int AttackCons;
		public int CurseCons;
		public int BlessCons;

	}

	public class DataBaseTemp
	{
		List<PlayerDB> Players;
		List<StatisticsDB> Statistics;
		StreamReader PlayersRead;
		StreamReader StatRead;
		StreamWriter PlayersWrite;
		StreamWriter StatWrite;
		public int AmountPlayers;

		public void Init() {
			Players = new List<PlayerDB> ();
			Statistics = new List<StatisticsDB> ();
			LoadInfo ();
			AmountPlayers = Players.Count;
		}

		public PlayerDB GetPlayerInfo(string Login) {
			foreach (PlayerDB item in Players) {
				if (item.Login == Login)
					return item;
			}
			return null;
		}

		public PlayerDB GetPlayerInfo(int Uid) {
			foreach (PlayerDB item in Players) {
				if (item.UserID == Uid)
					return item;
			}
			return null;
		}
		public void CreatePlayer(PlayerDB pl) {
			Players.Add (pl);
			Updateinfo ();
		}
		public void CreatePlayer(PlayerDB pl, StatisticsDB stat) {
			Players.Add (pl);
			Statistics.Add (stat);
			Updateinfo ();
		}

		public void LoadInfo() {
			PlayersRead = new StreamReader ("Players.txt");
			Players = JsonConvert.DeserializeObject<List<PlayerDB>> (PlayersRead.ReadToEnd ());
			StatRead = new StreamReader ("Stats.txt");
			Statistics = JsonConvert.DeserializeObject<List<StatisticsDB>> (StatRead.ReadToEnd ());
			PlayersRead.Close ();
			StatRead.Close ();
		}

		public void Updateinfo() {
			PlayersWrite = new StreamWriter ("Players.txt");
			PlayersWrite.WriteLine (JsonConvert.SerializeObject (Players));
			StatWrite = new StreamWriter ("Stats.txt");
			StatWrite.WriteLine (JsonConvert.SerializeObject (Statistics));
			PlayersWrite.Close ();
			StatWrite.Close ();
			AmountPlayers = Players.Count;
			
		}

		public StatisticsDB GetPlayerStat(int Uid) {
			foreach (StatisticsDB item in Statistics) {
				if (item.UserID == Uid)
					return item;
			}
			return null;
		}

		public  string Get(int OI, int Id)
		{
			throw new System.NotImplementedException();
		}

		public  void Set(int OI, List<String> Params)
		{
			throw new System.NotImplementedException();
		}

		public  void Create(int OI, List<String> Params)
		{
			throw new System.NotImplementedException();
		}

		public  void Update(int OI, List<String> Params)
		{
			throw new System.NotImplementedException();
		}

		public  void Delete(int OI, int Id)
		{
			throw new System.NotImplementedException();
		}

	}

}

