﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace Fan_Of_Worlds_Constructors_Server
{
	public class Duel
	{
		
		Server Ser;
		Stats Player1;
		Stats Player2;
		int[] Pl = new int[2];
		int pl1;
		int pl2;
		public Dictionary<int, int> FromIdToPl = new Dictionary<int, int> ();

		string LogMessage;

		public Duel (int Pl1, int Pl2, Server Serv)
		{
			Ser = Serv;
			Player1 = Ser.Clients [Pl1].St;
			Player2 = Ser.Clients [Pl2].St;
			pl1 = Pl1;
			pl2 = Pl2;
			Pl [0] = Pl1;
			Pl [1] = Pl2;
			FromIdToPl.Add (pl1, 0);
			FromIdToPl.Add (pl2, 0);
		}

		List<EntityController.Action> GlobalActions = new List<EntityController.Action>();
		List<int> ActionToPlayer = new List<int>();
		public int NowTurn = 0;
		List<EntityController.Interaction> GlobalInteractions = new List<EntityController.Interaction>();
		List<EntityController.Action> ShieldsP1 = new List<EntityController.Action>();
		List<EntityController.Action> ShieldsP2 = new List<EntityController.Action>();
		List<EntityController.Action> BlessP1 = new List<EntityController.Action>();
		List<EntityController.Action> BlessP2 = new List<EntityController.Action>();
		List<EntityController.Action> CurseP1 = new List<EntityController.Action>();
		List<EntityController.Action> CurseP2 = new List<EntityController.Action>();
		//List<EntityController.Action> ShieldsP1 = new List<EntityController.Action>();
		//List<EntityController.Action> ShieldsP2 = new List<EntityController.Action>();


		void CreateInteraction (int AId, int TargetId, int From, int To) {
			EntityController.Interaction cur = new EntityController.Interaction ();
			cur.Actor = GlobalActions [AId];
			if (To == 0) {
				cur.Target = Player1;
			} else {
				cur.Target = Player2;
			}
			GlobalInteractions.Add (cur);
		}

		public void BlankUpdate() {

			Ser.ServerManager.SendBattleInfo (Pl[NowTurn], Pl[(NowTurn + 1) % 2], string.Format("BattleBegins\n Now turn of Player {0}", Ser.Clients[Pl[NowTurn]].Login));
		}

		public void Update() {
			for (int i = 0; i < BlessP1.Count; ++i) {
				BlessP1 [i].Durability--;
				if (BlessP1 [i].Durability <= 0) {
					BlessP1.RemoveAt (i);
					i--;
					continue;
				}
			}
			for (int i = 0; i < BlessP2.Count; ++i) {
				BlessP2 [i].Durability--;
				if (BlessP2 [i].Durability <= 0) {
					BlessP2.RemoveAt (i);
					i--;
					continue;
				}
			}
			for (int i = 0; i < CurseP1.Count; ++i) {
				CurseP1 [i].Durability--;
				if (CurseP1 [i].Durability <= 0) {
					CurseP1.RemoveAt (i);
					i--;
					continue;
				}
			}
			for (int i = 0; i < CurseP2.Count; ++i) {
				CurseP2 [i].Durability--;
				if (CurseP2 [i].Durability <= 0) {
					CurseP2.RemoveAt (i);
					i--;
					continue;
				}
			}
			for (int i = 0; i < ShieldsP1.Count; ++i) {
				ShieldsP1 [i].Durability--;
				if (ShieldsP1 [i].Durability <= 0) {
					ShieldsP1.RemoveAt (i);
					i--;
					continue;
				}
			}
			for (int i = 0; i < ShieldsP2.Count; ++i) {
				ShieldsP2 [i].Durability--;
				if (ShieldsP2 [i].Durability <= 0) {
					ShieldsP2.RemoveAt (i);
					i--;
					continue;
				}
			}
			UpdateClasses ();
			UpdateInteractions ();
			Ser.ServerManager.SendBattleInfo (Pl[NowTurn], Pl[(NowTurn + 1) % 2], LogMessage);
		}

		public void ParseAndAct(List<List<int>> Scheme, int turn) {
			LogMessage = string.Format ("Now turn of Player {0}", Ser.Clients [Pl [NowTurn]].Login);
			List<EntityController.Action> Tmp = EntityController.Entity.ParseSchemeToActions(Scheme, (turn == 0) ? Player1 : Player2);
			foreach (EntityController.Action A in Tmp) {
				GlobalActions.Add (A);
				CreateInteraction (GlobalActions.Count - 1, 0, turn, (turn + 1) % 2);
			}
			NowTurn = turn;
			Update ();
		}

		void UpdateClasses() {
			for (int i = 0; i < GlobalInteractions.Count; ++i) {
				EntityController.Interaction Int = GlobalInteractions [i];
				switch (Int.Actor.AT) {
				case EntityController.ActionType.Blessing:
					{
						if (Int.Actor.IsAbsolute) {
							Int.Actor.St *= 0.1f;
						}
						(NowTurn == 0 ? BlessP1 : BlessP2).Add (Int.Actor);
						GlobalInteractions.RemoveAt (i);
						i--;
						break;
					}
				case EntityController.ActionType.Curse:
					{
						if (Int.Actor.IsAbsolute) {
							Int.Actor.St *= 0.1f;
						}
						(NowTurn == 0 ? CurseP1 : CurseP2).Add (Int.Actor);
						GlobalInteractions.RemoveAt (i);
						i--;
						break;
					}
				case EntityController.ActionType.Shield:
					{
						if (Int.Actor.IsAbsolute) {
							Int.Actor.St *= 0.1f;
						}
						(NowTurn == 0 ? ShieldsP1 : ShieldsP2).Add (Int.Actor);
						GlobalInteractions.RemoveAt (i);
						i--;
						break;
					}
				
				case EntityController.ActionType.None:
					{
						GlobalInteractions.RemoveAt (i);
						i--;
						break;
					}
				}
			}
		}

		void UpdateInteractions() {
			for (int i = 0; i < GlobalInteractions.Count; ++i) {
				EntityController.Interaction Int = GlobalInteractions [i];
				if (Int.Actor.Durability <= 0) {
					GlobalInteractions.RemoveAt (i);
					i--;
					continue;
				}
				if (Int.Actor.IsAbsolute) {
					Int.Actor.St = Int.Actor.St * 10;
					Int.Actor.IsAbsolute = false;
				}

				Int.Actor.Durability--;

				int DCS = 0;
				foreach (int DC in Int.Actor.DamageCoeffs) {
					DCS += DC;
				}
				Stats st = new Stats ();
				st.AddResist = Int.Actor.St.Resist();
				st.AddPhysDmg = Int.Actor.St.PhysDmg () * Int.Actor.DamageCoeffs [0] / DCS * (1 - (Int.Target.Defense() / 1500)) * (1 - Int.Target.PhysDmg() / 1000);
				st.AddAstDmg = Int.Actor.St.AstDmg () * Int.Actor.DamageCoeffs [1] / DCS * (1 - (Int.Target.Defense() / 1500)) * (1 - Int.Target.AstDmg() / 1000);
				st.AddMagDmg = Int.Actor.St.MagDmg () * Int.Actor.DamageCoeffs [2] / DCS * (1 - (Int.Target.Defense() / 1500)) * (1 - Int.Target.MagDmg() / 1000);
				foreach (EntityController.Action s in NowTurn == 0 ? BlessP1 : BlessP2) {
					if (s.IsAbsolute) {
						st += s.St;
					} else {
						st += st * (s.St * 0.01f);
					}
				}
				foreach (EntityController.Action s in NowTurn == 0 ? CurseP2 : CurseP1) {
					if (s.IsAbsolute) {
						st -= s.St;
					} else {
						st -= st * (s.St * 0.01f);
					}
				}
				foreach (EntityController.Action s in NowTurn == 0 ? ShieldsP2 : ShieldsP1) {
					if (s.IsAbsolute) {
						st -= s.St;
					} else {
						st -= st * (s.St * 0.01f);
					}
				}
				Stats.Resistance Tmp = Int.Target.Resist () - st.Resist ();
				float Damage = st.PhysDmg () + st.MagDmg () + st.AstDmg ();
				Damage -= Damage * 0.01f * (Tmp * true);
				if (NowTurn == 0) {
					Player2.AddHealth -= Damage;
					LogMessage += string.Format("Player {0} attack Player {1} and cause {2} damage\n", Ser.Clients[pl1].Login, Ser.Clients[pl2].Login, Damage);
					if (Player2.Health () <= 0) {

						LogMessage += string.Format("Player {0} lose battle", Ser.Clients[pl2].Login);
						Lose ();
					}
				} else {
					Player1.AddHealth -= Damage;
					LogMessage += string.Format("Player {0} attack Player {1} and cause {2} damage\n", Ser.Clients[pl2].Login, Ser.Clients[pl1].Login, Damage);
					if (Player1.Health () <= 0) {
						LogMessage += string.Format("Player {0} lose battle", Ser.Clients[pl1].Login);
						Lose ();
					}
				}
			}
		}

		void Lose() {

		}

	}
}

