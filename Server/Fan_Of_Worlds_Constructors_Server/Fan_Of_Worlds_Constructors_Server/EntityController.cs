
using System;
using System.Collections;
using System.Collections.Generic;
namespace Fan_Of_Worlds_Constructors_Server {
	public enum ConsBlock{
		Start, 		// 0

		//	Basics

		Shield, 	// 1
		Creature, 	// 2
		Attack, 	// 3
		Blessing, 	// 4
		Curse, 		// 5
		Construct, 	// 6

		// Splitters

		Splitter2, 	// 7
		Splitter3,	// 8
		Splitter4,	// 9

		// Energy

		Fire, 		// 10
		Water, 		// 11
		Ground, 		// 12
		Air, 		// 13
		light,   	// 14
		Dark, 		// 15
		Magic, 		// 16

		//Effects

		Arrow, 		// 17
		Orb, 		// 18
		Wall, 		// 19
		Area, 		// 20
		Multiply,	// 21
		// blocks below can't be main effect
		PhysicalD, 	// 22
		AstralD, 	// 23
		MagicD,		// 24
		Duration, 	// 25

	}




	public class Stats {

		public static int[] LevelsRate = { 1, 4, 20, 70, 250, 1000, 4000, 16000, 64000, 256000 };
		public static int[] BasicStatsRate = {5, 10, 15, 20, 25, 30, 35, 40, 45, 70};

		public class Resistance{
			public Resistance() {

			}

			public Resistance(float F, float W, float A, float E, float L, float D, float P, float As, float M){
				Fire = F;
				Water = W;
				Air = A;
				Earth = E;
				light = L;
				Dark = D;
				Physical = P;
				Astral = As;
				Magical = M;
			}

			public static Resistance operator + (Resistance F, Resistance Second) {
				return new Resistance (F.Fire + Second.Fire, 
					F.Water + Second.Water, 
					F.Air + Second.Air, 
					F.Earth + Second.Earth,
					F.light + Second.light,
					F.Dark + Second.Dark,
					F.Physical + Second.Physical,
					F.Astral + Second.Astral, 
					F.Magical + Second.Magical);
			}

			public static Resistance operator - (Resistance F, Resistance Second) {
				return new Resistance (F.Fire - Second.Fire, 
					F.Water - Second.Water, 
					F.Air - Second.Air, 
					F.Earth - Second.Earth,
					F.light - Second.light,
					F.Dark - Second.Dark,
					F.Physical - Second.Physical,
					F.Astral - Second.Astral, 
					F.Magical - Second.Magical);
			}

			public static Resistance operator * (Resistance F, Resistance Second) {
				return new Resistance (F.Fire * Second.Fire, 
					F.Water * Second.Water, 
					F.Air * Second.Air, 
					F.Earth * Second.Earth,
					F.light * Second.light,
					F.Dark * Second.Dark,
					F.Physical * Second.Physical,
					F.Astral * Second.Astral, 
					F.Magical * Second.Magical);
			}

			public static Resistance operator * (Resistance F, float Second) {
				return new Resistance (F.Fire * Second, 
					F.Water * Second, 
					F.Air * Second, 
					F.Earth * Second,
					F.light * Second,
					F.Dark * Second,
					F.Physical * Second,
					F.Astral * Second, 
					F.Magical * Second);
			}

			public static float operator * (Resistance F, bool S) {
				return  (F.Fire + 
					F.Water + 
					F.Air +
					F.Earth +
					F.light +
					F.Dark +
					F.Physical +
					F.Astral +
					F.Magical );
			}

			public static Resistance ToResist(float P) {
				return new Resistance (P, P, P, P, P, P, P, P, P);
			}
				
			public static Resistance ToResist(float F, float S) {
				return new Resistance (F, F, F, F, F, F, S, S, S);
			}

			public static Resistance ToResist(float F, float P, float A, float M) {
				return new Resistance (F, F, F, F, F, F, P, A, M);
			}

			public float Fire = 0f;
			public float Water = 0f;
			public float Air = 0f;
			public float Earth = 0f;
			public float light = 0f;
			public float Dark = 0f;
			public float Physical = 0f;
			public float Astral = 0f;
			public float Magical = 0f;
		}

		public Stats() {

		}

		public static Stats operator + (Stats F, Stats S) {
			Stats Res = new Stats ();
			Res.AddAgility = F.AddAgility + S.AddAgility;
			Res.AddAstDmg = F.AddAstDmg + S.AddAstDmg;
			Res.AddDefense = F.AddDefense + S.AddDefense;
			Res.AddHealth = F.AddHealth + S.AddHealth;
			Res.AddIntellect = F.AddIntellect + S.AddIntellect;
			Res.AddMagDmg = F.AddMagDmg + S.AddMagDmg;
			Res.AddPhysDmg = F.AddPhysDmg + S.AddPhysDmg;
			Res.AddResist = F.AddResist + S.AddResist;
			Res.AddStrength = F.AddStrength + S.AddStrength;
			Res.BaseAgility = F.BaseAgility + S.BaseAgility;
			Res.BaseIntellect = F.BaseIntellect + S.BaseIntellect;
			Res.BaseStrength = F.BaseStrength + S.BaseStrength;
			return Res;
		}

		public static Stats operator - (Stats F, Stats S) {
			Stats Res = new Stats ();
			Res.AddAgility = F.AddAgility - S.AddAgility;
			Res.AddAstDmg = F.AddAstDmg - S.AddAstDmg;
			Res.AddDefense = F.AddDefense - S.AddDefense;
			Res.AddHealth = F.AddHealth - S.AddHealth;
			Res.AddIntellect = F.AddIntellect - S.AddIntellect;
			Res.AddMagDmg = F.AddMagDmg - S.AddMagDmg;
			Res.AddPhysDmg = F.AddPhysDmg - S.AddPhysDmg;
			Res.AddResist = F.AddResist - S.AddResist;
			Res.AddStrength = F.AddStrength - S.AddStrength;
			Res.BaseAgility = F.BaseAgility - S.BaseAgility;
			Res.BaseIntellect = F.BaseIntellect - S.BaseIntellect;
			Res.BaseStrength = F.BaseStrength - S.BaseStrength;
			return Res;
		}

		public static Stats operator * (Stats F, Stats S) {
			Stats Res = new Stats ();
			Res.AddAgility = F.AddAgility * S.AddAgility;
			Res.AddAstDmg = F.AddAstDmg * S.AddAstDmg;
			Res.AddDefense = F.AddDefense * S.AddDefense;
			Res.AddHealth = F.AddHealth * S.AddHealth;
			Res.AddIntellect = F.AddIntellect * S.AddIntellect;
			Res.AddMagDmg = F.AddMagDmg * S.AddMagDmg;
			Res.AddPhysDmg = F.AddPhysDmg * S.AddPhysDmg;
			Res.AddResist = F.AddResist * S.AddResist;
			Res.AddStrength = F.AddStrength * S.AddStrength;
			Res.BaseAgility = F.BaseAgility * S.BaseAgility;
			Res.BaseIntellect = F.BaseIntellect * S.BaseIntellect;
			Res.BaseStrength = F.BaseStrength * S.BaseStrength;
			return Res;
		}

		public static Stats operator * (Stats F, float S) {
			Stats Res = new Stats ();
			Res.AddAgility = F.AddAgility * S;
			Res.AddAstDmg = F.AddAstDmg * S;
			Res.AddDefense = F.AddDefense * S;
			Res.AddHealth = F.AddHealth * S;
			Res.AddIntellect = F.AddIntellect * S;
			Res.AddMagDmg = F.AddMagDmg * S;
			Res.AddPhysDmg = F.AddPhysDmg * S;
			Res.AddResist = F.AddResist * S;
			Res.AddStrength = F.AddStrength * S;
			Res.BaseAgility = F.BaseAgility * S;
			Res.BaseIntellect = F.BaseIntellect * S;
			Res.BaseStrength = F.BaseStrength * S;
			return Res;
		}

		public Stats(float InitialValues, bool needInit) {
			if (needInit) {
				BaseStrength = InitialValues;
				BaseIntellect = InitialValues;
				BaseAgility = InitialValues;
			}
		}

		public Stats(int BaseLevel) {
			new Stats (BasicStatsRate [BaseLevel], true);
		}

		public float BaseStrength = 5;
		public float AddStrength;
		public float Strength() {
			return BaseStrength + AddStrength;
		}

		public float BaseAgility = 5;
		public float AddAgility;
		public float Agility() {
			return BaseAgility + AddAgility;
		}

		public float BaseIntellect = 5;
		public float AddIntellect;
		public float Intellect() {
			return BaseIntellect + AddIntellect;
		}

		public float CurrentAddHealth;
		public float AddHealth;
		public float MaxHealth() {
			return 10 * BaseStrength + AddHealth;
		}
		public float Health() {
			return MaxHealth () - CurrentAddHealth;
		}

		public float AddDefense;
		public float Defense() {
			return BaseAgility + AddAgility;
		}

		public Resistance AddResist;
		public Resistance Resist() {
			return Resistance.ToResist(Intellect() / 10, PhysDmg() / 10, AstDmg() / 10, MagDmg() / 10) + AddResist;
		}

		public float AddPhysDmg;
		public float PhysDmg() {
			return Strength () * 2 + AddPhysDmg;
		}

		public float AddAstDmg;
		public float AstDmg() {
			return Agility () * 2 + AddAstDmg;
		}

		public float AddMagDmg;
		public float MagDmg() {
			return Intellect () * 2 + AddMagDmg;
		}

	}

	public class EntityController
	{

		public enum ActionType {
			Attack, 
			Shield, 
			Blessing, 
			Curse, 
			Creature,
			None
		}

		public enum TargetType {
			Single, 
			Area, 
			Multiply, 
			Orb, 
			None
		}

		public struct Action
		{
			public ActionType AT;
			public TargetType TT;
			public Stats St;
			public String Name;
			public int Durability;
			public bool IsAbsolute;
			public int[] DamageCoeffs;

		}

		public struct Interaction
		{
			public Action Actor;
			public Stats Target;
		}

		static public int Type (ConsBlock block) {
			if (block == 0) {
				return 0;
			}
			if (block >= ConsBlock.Shield && block < ConsBlock.Splitter2)
				return 1;
			if (block >= ConsBlock.Splitter2 && block < ConsBlock.Fire)
				return 4;
			if (block >= ConsBlock.Fire && block < ConsBlock.Arrow)
				return 2;
			return 3;
		}

		static public int ConBlockAmount = 26;
			public class Entity
		{

			//Dictionary<ConsBlock, int> BlToInt = new Dictionary<ConsBlock, int>();
			public static Dictionary<int, ConsBlock> IntToBl = new Dictionary<int, ConsBlock>();
			public Entity() {
				if (IntToBl.Count <= 0) {
					IntToBl.Add(0, ConsBlock.Start);
					IntToBl.Add(1, ConsBlock.Shield);
					IntToBl.Add(2, ConsBlock.Creature);
					IntToBl.Add(3, ConsBlock.Attack);
					IntToBl.Add(4, ConsBlock.Blessing);
					IntToBl.Add(5, ConsBlock.Curse);
					IntToBl.Add(6, ConsBlock.Construct);
					IntToBl.Add(7, ConsBlock.Splitter2);
					IntToBl.Add(8, ConsBlock.Splitter3);
					IntToBl.Add(9, ConsBlock.Splitter4);
					IntToBl.Add(10, ConsBlock.Fire);
					IntToBl.Add(11, ConsBlock.Water);
					IntToBl.Add(12, ConsBlock.Ground);
					IntToBl.Add(13, ConsBlock.Air);
					IntToBl.Add(14, ConsBlock.light);
					IntToBl.Add(15, ConsBlock.Dark);
					IntToBl.Add(16, ConsBlock.Magic);
					IntToBl.Add(17, ConsBlock.Arrow);
					IntToBl.Add(18, ConsBlock.Orb);
					IntToBl.Add(19, ConsBlock.Wall);
					IntToBl.Add(20, ConsBlock.Area);
					IntToBl.Add(21, ConsBlock.Multiply);
					IntToBl.Add(22, ConsBlock.PhysicalD);
					IntToBl.Add(23, ConsBlock.AstralD);
					IntToBl.Add(24, ConsBlock.MagicD);
					IntToBl.Add(25, ConsBlock.Duration);
				}
			}
			public Stats St = new Stats();

			public struct Node
			{
				public int Id;
				public ConsBlock Type;
				public int Parent;
				public int Branch;
				public List<int> Childs;
				public Node(int _id, ConsBlock _tp, int branch, int parent) {
					Id = _id;
					Type = _tp;
					Parent = parent;
					Branch = branch;
					Childs = new List<int>();
				}
			}	
			public List<Node> Nodes = new List<Node>();

			public List<int> IdToLevels = new List<int>();
			public List<List<int>> Scheme; // Level: 0 - Basic Type, 1 - Main Effect, 2 - Main Energy
			public List<int> Distances = new List<int>();

			int FindLastInBranch(int brId) {
				int id = -1;
				foreach (Node item in Nodes) {
					if (item.Branch == brId && item.Id >= id)
						id = item.Id;
				}
				return id;
			}

			public void AddBlock(int ID, int brId, int PId = -1) {
				if (PId < 0)
					PId = FindLastInBranch (brId);
				Nodes.Add (new Node (Nodes.Count, IntToBl[ID], brId, PId));
				if (PId >= 0)
					Nodes [PId].Childs.Add (Nodes.Count - 1);
				Distances.Add (0);
				Parse ();
			}

			public void DeleteBlock(int Id) {
				if (Nodes [Id].Childs.Count <= 0) {
					Nodes [Nodes [Id].Parent].Childs.Remove (Id);
					Nodes.RemoveAt (Id);
					Distances.RemoveAt (Id);
					Parse ();
				}
			}


			public bool IsSpecific = false;
			public bool HaveOwnCons = false;
			public List<Entity> OwnCons;
			public int Levels = 1;
			public int Branchs = 1;
			public ConsBlock MainEnergy;
			List<int> EnergiesCount = new List<int>(6);

			public void Parse() {
				Levels = 1;
				MaxDist = 0;
				Branchs = 1;
				Scheme = new List<List<int>> ();
				List<int> Tmp = new List<int> (ConBlockAmount + 3);
				for (int i = 0; i < ConBlockAmount + 3; i++) 
					Tmp.Add(0);
				Scheme.Add (Tmp);
				if(Nodes[0].Childs.Count > 0)
					Parse (0, 1, 0);
			}


			static int FindMaxIndex(List<int> l) {
				int Ind = -1;
				int Max = -1;
				for (int i = 0; i < l.Count; i++) {
					if (l [i] > Max) {
						Ind = i;
						Max = l [i];
					}
				}
				return Ind;
			}


			static void FindMainEnergy(ref List<int> EnergiesCount, ref List<List<int>> Scheme, ref ConsBlock MainEnergy) {
				EnergiesCount = new List<int>(6);
				for (int i = 0; i < 6; ++i) {
					EnergiesCount.Add (0);
				}
				List<int> SubEnergiesCount = new List<int> ();
				for (int i = 0; i < Scheme.Count; ++i) {
						for (int j = 0; j < 6; j++) {
							EnergiesCount[j] += Scheme [i] [j + 10];
						}
				}
				SubEnergiesCount.Add(EnergiesCount[0] - EnergiesCount [1]);
				SubEnergiesCount.Add(EnergiesCount[1] - EnergiesCount [0]);
				SubEnergiesCount.Add(EnergiesCount[2] - EnergiesCount [3]);
				SubEnergiesCount.Add(EnergiesCount[3] - EnergiesCount [2]);
				SubEnergiesCount.Add(EnergiesCount[4] - EnergiesCount [5]);
				SubEnergiesCount.Add(EnergiesCount[5] - EnergiesCount [4]);
				int I = FindMaxIndex (SubEnergiesCount);
				Entity tmp = new Entity ();
				MainEnergy = Entity.IntToBl[I + 10];
			}

			public int MaxDist = 0;

			void Parse (int Level, int Id, int Dist) {
				ParseDfs (Level, Id, Dist);
			}

			void ParseDfs(int Level, int Id, int Dist) {
				Dist++;
				Distances [Id] = Dist;
				if (Dist > MaxDist)
					MaxDist = Dist;
				if (Id == 0) {
					Parse (Level, 1, 0);
					return;
				}
				int t = Type (Nodes [Id].Type);
				switch (t) {
				case 0:
					{
						Levels++;
						Scheme.Add (new List<int> (ConBlockAmount + 3));
						for (int i = 0; i < ConBlockAmount + 3; i++) 
							Scheme[Levels - 1].Add(0);
						Scheme [Levels - 1] [0] = -1;
						if (Nodes [Id].Childs.Count > 0) {
							ParseDfs (Level, Nodes [Id].Childs [0], Dist);
						}
						break;
					}
				case 1:
					{
						Levels++;
						Scheme.Add (new List<int> (ConBlockAmount + 3));
						for (int i = 0; i < ConBlockAmount + 3; i++) 
							Scheme[Levels - 1].Add(0);
						Scheme [Levels - 1] [0] = -1;
						Scheme [Levels - 1][0] = (int)Nodes [Id].Type;
						Scheme [Levels - 1] [1] = -1;
						if (Nodes[Id].Childs.Count > 0)
							ParseDfs (Level, Nodes [Id].Childs [0], Dist);
						break;
					}
				case 2:
					{
						Scheme [Level] [(int)(Nodes [Id].Type)]++;
						if (Nodes[Id].Childs.Count > 0)
							ParseDfs (Level, Nodes [Id].Childs [0], Dist);
						break;
					}
				case 3:
					{
						if (Scheme [Level] [0] == -1) {
							if (Scheme [0] [1] == -1) {
								if ((int)Nodes [Id].Type < (int)ConsBlock.PhysicalD) {
									Scheme [Level] [1] = (int)Nodes [Id].Type;
								}
							}
						}
						if (Scheme [Level] [1] == -1) {
							if ((int)Nodes [Id].Type < (int)ConsBlock.PhysicalD) {
								Scheme [Level] [1] = (int)Nodes [Id].Type;
							}
						}
						Scheme [Level] [(int)Nodes [Id].Type]++;
						if (Nodes[Id].Childs.Count > 0)
							ParseDfs (Level, Nodes [Id].Childs [0], Dist);
						break;
					}
				case 4:
					{
						Branchs += (int)Nodes [Id].Type - 6;
						foreach (int n in Nodes[Id].Childs) {
							//Branchs++;
							Parse (Level, n, Dist);
						}
						break;
					}
				}


			}

			public static float[] FireInf = {0.5f, 0.1f, 0.3f, 2, -2, -1, 1, 1, -1};
			public static float[] WaterInf = {0.3f, 0.5f, 0.1f, -2, 2, 1, -1, -1, 1};
			public static float[] EarthInf = {0.5f, 0.1f, 0.3f, -1, 1, 2, -2, -1, 1};
			public static float[] AirInf = {0.3f, 0.5f, 0.1f, 1, -1, -2, 2, 1, -1};
			public static float[] LightInf = {0.1f, 0.3f, 0.5f, 1, -1, -1, 1, 2, -2};
			public static float[] DarkInf = {0.1f, 0.3f, 0.5f, -1, 1, 1, -1, -2, 2};
			public static float[] ArrowInf = {3, 2, 1, 2, 2, 2, 2, 2, 2};
			public static float[] OrbInf = {1, 2, 3, 2, 2, 2, 2, 2, 2};
			public static float[] AreaInf = {2, 2, 2, 2, 2, 2, 2, 2, 2};
			public static float[] MultiplyInf = {3, 3, 3, 2, 2, 2, 2, 2, 2};
			public static float[] PhysInf = {5, 0, 0, 0, 0, 0, 0, 0, 0};
			public static float[] AstInf = {0, 5, 0, 0, 0, 0, 0, 0, 0};
			public static float[] MagInf = {0, 0, 5, 0, 0, 0, 0, 0, 0};

			static float FromEnergyToFloat(float[] coeffs, List<int> en, int StartIndex, int Count) {
				float Res = 0f;
				for (int i = StartIndex; i < StartIndex + Count; ++i) {
					Res += coeffs [i] * en [i];
				}
				return Res;
			}
			static float[] f0 = { FireInf [0], WaterInf [0], EarthInf [0], AirInf [0], LightInf [0], DarkInf [0] };
			static float[] f1 = { FireInf [1], WaterInf [1], EarthInf [1], AirInf [1], LightInf [1], DarkInf [1] };
			static float[] f2 = { FireInf [2], WaterInf [2], EarthInf [2], AirInf [2], LightInf [2], DarkInf [2] };
			static float[] f3 = { FireInf [3], WaterInf [3], EarthInf [3], AirInf [3], LightInf [3], DarkInf [3] };
			static float[] f4 = { FireInf [4], WaterInf [4], EarthInf [4], AirInf [4], LightInf [4], DarkInf [4] };
			static float[] f5 = { FireInf [5], WaterInf [5], EarthInf [5], AirInf [5], LightInf [5], DarkInf [5] };
			static float[] f6 = { FireInf [6], WaterInf [6], EarthInf [6], AirInf [6], LightInf [6], DarkInf [6] };
			static float[] f7 = { FireInf [7], WaterInf [7], EarthInf [7], AirInf [7], LightInf [7], DarkInf [7] };
			static float[] f8 = { FireInf [8], WaterInf [8], EarthInf [8], AirInf [8], LightInf [8], DarkInf [8] };

			static Stats EnergyToStat(ref List<int> en, int StartIndex) {
				Stats Res = new Stats (0);
				Res.AddResist = new Stats.Resistance ();
				Res.AddPhysDmg = FromEnergyToFloat(f0, en, StartIndex, 6);
				Res.AddAstDmg = FromEnergyToFloat(f1, en, StartIndex, 6);
				Res.AddMagDmg = FromEnergyToFloat(f2, en, StartIndex, 6);
				Res.AddResist.Fire = FromEnergyToFloat(f3, en, StartIndex, 6);
				Res.AddResist.Water = FromEnergyToFloat(f4, en, StartIndex, 6);
				Res.AddResist.Earth = FromEnergyToFloat(f5, en, StartIndex, 6);
				Res.AddResist.Air = FromEnergyToFloat(f6, en, StartIndex, 6);
				Res.AddResist.light = FromEnergyToFloat(f7, en, StartIndex, 6);
				Res.AddResist.Dark = FromEnergyToFloat(f8, en, StartIndex, 6);
				return Res;
			}

			static Stats EnergyToStat(ref List<List<int>> en, int ListIndex, int StartIndex) {
				Stats Res = new Stats (0);
				Res.AddResist = new Stats.Resistance ();
				Res.AddPhysDmg = FromEnergyToFloat(f0, en[ListIndex], StartIndex, 6);
				Res.AddAstDmg = FromEnergyToFloat(f1, en[ListIndex], StartIndex, 6);
				Res.AddMagDmg = FromEnergyToFloat(f2, en[ListIndex], StartIndex, 6);
				Res.AddResist.Fire = FromEnergyToFloat(f3, en[ListIndex], StartIndex, 6);
				Res.AddResist.Water = FromEnergyToFloat(f4, en[ListIndex], StartIndex, 6);
				Res.AddResist.Earth = FromEnergyToFloat(f5, en[ListIndex], StartIndex, 6);
				Res.AddResist.Air = FromEnergyToFloat(f6, en[ListIndex], StartIndex, 6);
				Res.AddResist.light = FromEnergyToFloat(f7, en[ListIndex], StartIndex, 6);
				Res.AddResist.Dark = FromEnergyToFloat(f8, en[ListIndex], StartIndex, 6);
				return Res;
			}


			Stats ModifierToStat(ConsBlock Type) {
				Stats Res = new Stats ();

				return Res;
			}



			public static List<Action> ParseSchemeToActions(List<List<int>> Scheme, Stats st) {
				List<Action> Res = new List<Action>();
				List<int> EnergiesCount = new List<int>();
				ConsBlock MainEnergy = ConsBlock.Air;
				FindMainEnergy (ref EnergiesCount,  ref Scheme, ref MainEnergy);
				TargetType Current = TargetType.None;
				st = st + EnergyToStat (ref EnergiesCount, 0);
				for (int i = 0; i < Scheme.Count; ++i) {
					if (Scheme [i] [0] != -1) {
						Action Cur = new Action ();
						Cur.IsAbsolute = false;
						switch (Scheme [i] [0]) {
							case 1:{ Cur.AT = ActionType.Shield; break;}
							case 2:{ Cur.AT = ActionType.Creature; break;}
							case 3:{ Cur.AT = ActionType.Attack; break;}
							case 4:{ Cur.AT = ActionType.Blessing; break;}
							case 5:{ Cur.AT = ActionType.Curse; break;}
							default:
								continue;
						}
						Cur.IsAbsolute = Scheme [i] [19] > 0;
						if (Current == TargetType.None) {
							switch (Scheme [i] [1]) {
							case 17:
								{
									Current = TargetType.Single;
									break;}
							case 18:
								{
									Current = TargetType.Orb;
									break;}
							case 20:
								{
									Current = TargetType.Area;
									break;}
							case 21:
								{
									Current = TargetType.Multiply;
									break;}
							default:
								{
									Current = TargetType.None;
									continue;}
							}
						}
						Cur.St = Cur.St + st + EnergyToStat (ref Scheme, i, 10);
						Cur.DamageCoeffs = new int[3]; 
						Cur.DamageCoeffs [0] = Scheme [i] [22];
						Cur.DamageCoeffs [1] = Scheme [i] [23];
						Cur.DamageCoeffs [2] = Scheme [i] [24];
						Cur.Durability = (Cur.AT == ActionType.Attack || Cur.AT == ActionType.Creature) ? 0 : 2 + Scheme [i] [25];
						Cur.TT = Current;
						Res.Add (Cur);
					}
				}
				for (int i = 0; i < Res.Count; ++i) {
					//Res [i].TT = Current;
					//Доделать добавление по модификатору или нет?
				}
				return Res;
			}

		}


		public Entity Current = new Entity();
			
			public EntityController ()
			{
			}


	}
}


