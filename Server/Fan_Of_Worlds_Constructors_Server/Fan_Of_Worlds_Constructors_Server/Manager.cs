﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан инструментальным средством
//     В случае повторного создания кода изменения, внесенные в этот файл, будут потеряны.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Fan_Of_Worlds_Constructors_Server;

public class Manager
{
	

	public Server Serv;

	public struct MIdent {
		public int Mid;
		public int SID;
	}

	public class PlayerInfo {
		public int UserId;
		public string Login;
		public string Password;
		public int Level;
		public int Exp;
		public string Email;
	}

	public class ChatInfo {
		public int ChanellId;
		public String Message;
		public bool findingPartner;
	}

	public class OnlineInfo {
		public bool findingPartner;
		public int PlayersOnline;
		public int PlayersInQueue;
	}

	public class BattleInfo {
		public float Health;
		public int ActionPoints;
		public int NowTurnPId;
		public string Log;
	}

	public class StatsInfo {
		public int MatchesPlayed { get; set; }
		public int Wins { get; set;} 
		public int DiffrentConsUsed{get; set;}
		public int CreatureCons{get; set;}
		public int ShieldCons{get; set;}
		public int AttackCons{get; set;}
		public int CurseCons{get; set;}
		public int BlessCons{get; set;}
		public int Strenght{get; set;}
		public int Intellect{get; set;}
		public int Agility{get; set;}
	}

	public class SwitchTurnInfo {
		public List<List<int>> Scheme;
		int Target;
	}

	public class Datagramm {
		public MIdent MI = new MIdent();
		public PlayerInfo PI = null;
		public ChatInfo CI = null;
		public OnlineInfo OI = null;
		public BattleInfo PlBI = null;
		public BattleInfo EnBI = null;
		public SwitchTurnInfo STI = null;
		public StatsInfo SI = null;
	}


	public void Init() {

	}

	void AddingToPlayerQueue(Datagramm Input, int Sid) {
		if (Input.OI.findingPartner) {
			AddToQueue (Sid);
		} else {
			PlayersQueue.Remove (Sid);
		}
	}

	List<int> PlayersQueue = new List<int> ();
	Dictionary<int, int> PairsInDuels = new Dictionary<int, int>();
	List<Duel> Duels = new List<Duel>();
	Dictionary<int, int> FromPIdToDuel = new Dictionary<int, int>();
	bool PlayersComparing(int pl1, int pl2) {
		return true;
	}

	void AddToQueue(int Id) {
		Id = Serv.Clients [Id].Id;
		for (int i = 0; i < PlayersQueue.Count;  ++i) {
			var pl = PlayersQueue [i];
			if (PlayersComparing(Id, pl)) {
				PairsInDuels.Add (Id, pl);
				PairsInDuels.Add (pl, Id);
				Duel Tmp = new Duel (Id, pl, Serv);
				FromPIdToDuel.Add (pl, Duels.Count);
				FromPIdToDuel.Add (Id, Duels.Count);
				Duels.Add (Tmp);
				SendBattleInfo (Id, pl, "StartBattle");
				SendStartBattleMessage (Id, pl);
				Duels [FromPIdToDuel [Id]].BlankUpdate ();
				PlayersQueue.Remove(pl);
				return;
			}
		}
		PlayersQueue.Add (Id);	
	}

	public void UpdatePlayerInfo(Client Player) {

	}


	String Serialize(object obj) {
		return JsonConvert.SerializeObject (obj);
	}

	Datagramm Deserialize(String Msg) {
		return JsonConvert.DeserializeObject<Datagramm> (Msg);
	}

	public void StartCommunication(int SID) {
		Client Player = Serv.Clients [SID];
		Player.SessionID = SID;
		Player.StartConnection ();
//		InitialDatagramm ID = new InitialDatagramm ();
//		ID.Mid = 0;
//		ID.SID = SID;
//		String Msg = Serialize (ID);
//		Console.WriteLine ("Send message to client {0}: {1}", SID, Msg);
//		Player.SendMessage (Msg, 0);
	}

	public void AddInfoAndSend(Datagramm Data, int Mid, int Uid) {
		Data.MI.Mid = Mid;
		Data.MI.SID = Uid;
		String Msg = Serialize (Data);
		Console.WriteLine ("Send message to client {0}: {1}", Uid, Msg);
		Serv.Clients[Uid].SendMessage (Msg);
	}

	public void SendBlankMsg(int Mid, int id) {
		Datagramm Out = new Datagramm ();
		Out.MI.Mid = Mid;
		Out.MI.SID = id;
		String Msg = Serialize (Out);
		if (Mid != 0)
			Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
	}

	public void SendInitialInfo (int id) {
		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 1;
		ID.MI.SID = id;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
	}

	public void SendEmptyGuest (int id) {
		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 2;
		ID.MI.SID = id;
		ID.PI = new PlayerInfo ();
		ID.PI.Login = System.String.Format ("Guest{0}", id);
		ID.PI.Exp = 0;
		ID.PI.Level = 0;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
		}

	public void SendChatMessageToAllBeside (Datagramm Data, int Beside) {
		foreach (Client player in Serv.Clients) {
			if (player.SessionID != Beside && player.IsConnected()) {
				AddInfoAndSend (Data, 4, player.SessionID);
			}
		}
	}

	public void SendChatMessageToGroupBeside (Datagramm Data, int Group, int Beside) {

	}

	public void RegisterPlayer(Datagramm Data, int id) {
		lock (Serv.CreatingPlayer) {
			Serv.Clients [id].Login = Data.PI.Login;
			Serv.Clients [id].Password = Data.PI.Password;
			Serv.Clients [id].Exp = 0;
			Serv.Clients [id].Level = 0;
			Serv.Clients [id].Id = Serv.DataBase.AmountPlayers;
			Serv.DataBase.CreatePlayer (Data.PI.Login, Data.PI.Password, Data.PI.Email);
			SendAuthoRequest (id);
		}
	
	}

	public void AuthorisatePlayer(Datagramm Data, int id) {
		PlayerDB pl = Serv.DataBase.GetPlayerInfo (Data.PI.Login);
		if (pl == null) {
			return;
		}
		if (pl.Password.Equals(Data.PI.Password)) {
			Serv.Clients [id].Id = pl.UserId;
			Serv.Clients [id].Login = pl.Login;
			Serv.Clients [id].Password = pl.Password;
			Serv.Clients [id].Exp = pl.Exp;
			Serv.Clients [id].Level = pl.Level;
			Serv.Clients [id].Email = pl.Email;
			Serv.Clients [id].St = Serv.DataBase.GetPlayerPrefs(id).ToStat();
			Serv.Clients [id].St.AddResist = new Stats.Resistance ();
			SendAuthoRequest (id);
		}
	}

	public void SendAuthoRequest(int id) {
		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 6;
		ID.MI.SID = id;
		ID.PI = new PlayerInfo ();
		ID.PI.Login = Serv.Clients [id].Login;
		ID.PI.Exp = Serv.Clients [id].Exp;
		ID.PI.Level = Serv.Clients [id].Level;
		ID.PI.Email = Serv.Clients [id].Email;
		ID.PI.Password = Serv.Clients [id].Password;
		ID.PI.UserId = Serv.Clients [id].Id;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
	}

	public void SendPlayerInfo(int Sid,  int Uid) {
		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 6;
		ID.MI.SID = Sid;
		ID.PI = new PlayerInfo ();
		ID.PI.Login = Serv.Clients [Uid].Login;
		ID.PI.Exp = Serv.Clients [Uid].Exp;
		ID.PI.Level = Serv.Clients [Uid].Level;
		ID.PI.UserId = Serv.Clients [Uid].Id;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", Sid, Msg);
		Serv.Clients[Sid].SendMessage (Msg);
	}

	public void SendStartBattleMessage(int pl1, int pl2) {
		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 12;
		ID.MI.SID = pl1;
		ID.PI = new PlayerInfo ();
		ID.PI.Login = Serv.Clients [pl2].Login;
		ID.PI.Exp = Serv.Clients [pl2].Exp;
		ID.PI.Level = Serv.Clients [pl2].Level;
		ID.PI.UserId = Serv.Clients [pl2].Id;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", pl1, Msg);
		Serv.Clients[pl1].SendMessage (Msg);

		ID.MI.SID = pl2;
		ID.PI = new PlayerInfo ();
		ID.PI.Login = Serv.Clients [pl1].Login;
		ID.PI.Exp = Serv.Clients [pl1].Exp;
		ID.PI.Level = Serv.Clients [pl1].Level;
		ID.PI.UserId = Serv.Clients [pl1].Id;
		Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", pl2, Msg);
		Serv.Clients[pl2].SendMessage (Msg);
	}

	public void SendOnlineInfo(int id) {
		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 13;
		ID.MI.SID = id;
		ID.OI = new OnlineInfo ();
		ID.OI.PlayersOnline = Serv.Clients.Count;
		ID.OI.PlayersInQueue = PlayersQueue.Count;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
	}

	public void SendBattleInfo(int pl1, int pl2, string Log) {
		BattleInfo Pl1 = new BattleInfo();
		Pl1.Health = Serv.Clients [pl1].St.Health ();
		Pl1.ActionPoints = Serv.Clients [pl1].AP;
		Pl1.NowTurnPId = pl1;

		BattleInfo Pl2 = new BattleInfo ();
		Pl2.Health = Serv.Clients [pl2].St.Health ();
		Pl2.ActionPoints = Serv.Clients [pl2].AP;
		Pl2.NowTurnPId = pl1;
		SendBattleInfo (pl1, Pl1, Pl2, Log);
		SendBattleInfo (pl2, Pl2, Pl1, Log);
	}

	public void SendBattleInfo(int id, BattleInfo PlBi, BattleInfo EnBi, string Log) {

		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 8;
		ID.MI.SID = id;
		ID.PlBI = PlBi;
		ID.EnBI = EnBi;
		ID.PlBI.Log = Log;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
	}

	public void SendStatsInfo(int id) {
		Datagramm ID = new Datagramm ();
		ID.MI.Mid = 14;
		ID.MI.SID = id;
		StatisticsDB SDB = Serv.DataBase.GetPlayerStat (id);
		StatsInfo S = new StatsInfo ();
		S.Agility = (int)Serv.Clients [id].St.BaseAgility;
		S.Strenght = (int)Serv.Clients [id].St.BaseStrength;
		S.Intellect = (int)Serv.Clients [id].St.BaseIntellect;
		S.MatchesPlayed = SDB.MatchesPlayed;
		S.Wins = SDB.Wins;
		S.CreatureCons = SDB.CreatureCons;
		S.AttackCons = SDB.AttackCons;
		S.BlessCons = SDB.BlessCons;
		S.CurseCons = SDB.CurseCons;
		S.DiffrentConsUsed = SDB.DiffrentConsUsed;
		S.ShieldCons = SDB.ShieldCons;
		ID.SI = S;
		String Msg = Serialize (ID);
		Console.WriteLine ("Send message to client {0}: {1}", id, Msg);
		Serv.Clients[id].SendMessage (Msg);
	}

	public void ExecuteMessage (String Msg, int id) {
		List<String> Msgs = new List<string> (Msg.Split ('\0'));
		foreach (String Message in Msgs) {
			if (Message.Length <= 0)
				continue;
			Datagramm Input = Deserialize (Message);
			switch (Input.MI.Mid) {
			case 1:
				{
					SendInitialInfo (id);
					break;
				}
			case 2:
				{
					SendEmptyGuest (id);
					break;
				}
			case 3:
				{
					SendChatMessageToAllBeside (Input, id);
					break;
				}
			case 5:
				{
					RegisterPlayer (Input, id);
					break;
				}
			case 6:
				{
					AuthorisatePlayer (Input, id);
					break;
				}
			case 7:
				{
					//Serv.Clients [PairsInDuels [id]].St.AddHealth -= 20;
					Duels[FromPIdToDuel[id]].ParseAndAct(Input.STI.Scheme, Duels[FromPIdToDuel[id]].FromIdToPl[id]);
					//SendBattleInfo (id, PairsInDuels [id]);
					break;
				}
			case 11:
				{
					AddingToPlayerQueue (Input, id);
					break;
				}
			case 13:
				{
					SendOnlineInfo (id);
					break;
				}
			case 14:
				{
					SendStatsInfo (id);
					break;
				}
			default:
				{
					break;
				}
			}
		}
	}

}

