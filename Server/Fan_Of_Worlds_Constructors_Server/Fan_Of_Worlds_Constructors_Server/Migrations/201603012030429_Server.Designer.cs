// <auto-generated />
namespace Fan_Of_Worlds_Constructors_Server.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.0-20911")]
    public sealed partial class Server : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Server));
        
        string IMigrationMetadata.Id
        {
            get { return "201603012030429_Server"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
